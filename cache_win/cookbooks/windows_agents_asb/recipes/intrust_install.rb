#
# Cookbook Name:: windows_agents_asb
# Recipe:: intrust_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe Intrust Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "intrust_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Check pre-requisites
execute 'start_windows_installer' do
  command "sc start msiserver"
  not_if 'sc query msiserver | find "RUNNING"'
end

#Check pre-requisites
windows_service 'msiserver' do
  action :start
end

#Copy installers
powershell_script "copy installers" do
 code <<-EOH
 Copy-Item -Path Z:\\Agentes\\Intrust -Destination C:\\tmp -recurse
 EOH
 not_if "Test-Path C:\\tmp\\Intrust"
end

#Install Intrust
batch "intrust_install" do
  cwd "C:\\tmp\\Intrust"
  code <<-EOH
  Setup_WindowsAgent.cmd
  EOH
  not_if "sc query adcscm"
  notifies :start, 'windows_service[adcscm]', :delayed
end

#start the service
windows_service 'adcscm' do
  action :nothing
end

#Intrust Register
include_recipe "windows_agents_asb::intrust_register"