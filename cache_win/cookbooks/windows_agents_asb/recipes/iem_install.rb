#
# Cookbook Name:: windows_os_asb
# Recipe:: iem_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe IEM Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "iem_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip

#Check pre-requisites
execute 'start_windows_installer' do
  command "sc start msiserver"
  not_if 'sc query msiserver | find "RUNNING"'
end

#copy installers
case ambiente
  when 'Produccion'
	powershell_script "copy_installers" do
	 code <<-EOH
	   Copy-Item -Path Z:\\Agentes\\IEM\\PROD\\* -Destination C:\\tmp -recurse
	   EOH
	   not_if "Test-Path C:\\tmp\\setup.exe"
	end
  when 'Certificacion'
    powershell_script "copy_installers" do
	 code <<-EOH
	   Copy-Item -Path Z:\\Agentes\\IEM\\CERT\\* -Destination C:\\tmp -recurse
	   EOH
	   not_if "Test-Path C:\\tmp\\setup.exe"
	end
  when 'Desarrollo'
	powershell_script "copy_installers" do
	 code <<-EOH
	   Copy-Item -Path Z:\\Agentes\\IEM\\DESA\\* -Destination C:\\tmp -recurse
	   EOH
	   not_if "Test-Path C:\\tmp\\setup.exe"
	end
end  

#run installer
batch "IEM_Install" do
  code <<-EOH
  Start /wait C:\\tmp\\setup.exe /S /v/qn
  EOH
  not_if "sc query besclient"
end
