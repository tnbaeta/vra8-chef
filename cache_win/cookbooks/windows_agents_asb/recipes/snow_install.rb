#
# Cookbook Name:: windows_agents_asb
# Recipe:: AgentSnow_install
# Author:: Joel Rodriguez Cerin
# Copyright 2018, IBM
#
# Fecha Version:: 07.12.2018
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe Snow Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "snow_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os=node['kernel']['os_info']['caption'].strip
ip=node['ipaddress'].gsub(/\s+/, "")

#Check pre-requisites
execute 'start_windows_installer' do
  command "sc start msiserver"
  not_if 'sc query msiserver | find "RUNNING"'
end

#copy installers
powershell_script "copy_installers" do
	code <<-EOH
	Copy-Item -Path Z:\\Agentes\\Snow\\RedesLANTAN\\Windows\\* -Destination C:\\tmp -recurse
	EOH
	not_if "Test-Path C:\\tmp\\bancodecreditodelperu_snowagent531_x64.msi"
end

#Install Snow Agent
execute "install_snow_agent" do
  command <<-EOF
  msiexec.exe /i C:\\tmp\\bancodecreditodelperu_snowagent531_x64.msi /l*v C:\\tmp\\SnowAgentinstall.log /qn
  EOF
  not_if "sc query Snow Inventory Client"
end
