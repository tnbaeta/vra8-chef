#
# Cookbook Name:: windows_os_asb
# Recipe:: intrust_register
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe Intrust Register" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "intrust_register",Chef.run_context.node.run_list
    )
  end
end

#load credentials from data bag
secure = Chef::EncryptedDataBagItem.load("credentials", "intrust","K6rk4wLijYMvxQJS")
password = secure["secret"]

#load server names from data bag
intrustServers = data_bag_item("recursos","intrust")
server01 = intrustServers["server01"]
server02 = intrustServers["server02"]

#Check if agents are registered
powershell_script 'check_intrust_registration' do
  cwd "C:\\WINDOWS\\ADCAGENT"
  code <<-EOH
  $server01=(.\\adcscm.nt_intel.exe -list | select-string #{server01})
  $server02=(.\\adcscm.nt_intel.exe -list | select-string #{server01})
  if(($server01 -like "*#{server01}*") -and ($server02 -like "*#{server02}*")){
    New-Item (.\\adcscm.nt_intel.exe -list | select-string #{server01} | foreach-object { $_ -split ":"} | Select-Object -Last 1).trim() -type File -Force
    New-Item (.\\adcscm.nt_intel.exe -list | select-string #{server02} | foreach-object { $_ -split ":"} | Select-Object -Last 1).trim() -type File -Force
  }
  EOH
  not_if { File.exist?("C:\\WINDOWS\\ADCAGENT\\#{server01}") }
end

#Register Intrust agent
powershell_script 'register_agent_on_node_1' do
  guard_interpreter :powershell_script
  cwd "C:\\WINDOWS\\ADCAGENT"
  code <<-EOH
  while($true){
  .\\adcscm.nt_intel.exe -add #{server01} 900 #{password}
  if($LASTEXITCODE -eq 0){
    Write-host "Success registering agent on node #{server01}"
    break
   } else {
     Write-host "Error registering agent on node #{server01}"
   }
   Start-sleep -s 5
   Write-host "Codigo de Error" $LASTEXITCODE
  }
  EOH
  not_if "test-path C:\\WINDOWS\\ADCAGENT\\#{server01}"
end

powershell_script 'register_agent_on_node_2' do
  guard_interpreter :powershell_script
  cwd "C:\\WINDOWS\\ADCAGENT"
  code <<-EOH
  while($true){
  .\\adcscm.nt_intel.exe -add 172.29.103.14 900 #{password}
  if($LASTEXITCODE -eq 0){
    Write-host "Success registering agent on node #{server02}"
    break
   } else {
     Write-host "Error registering agent on node #{server02}"
   }
   Start-sleep -s 5
   Write-host "Codigo de Error" $LASTEXITCODE
  }
  EOH
  not_if "test-path C:\\WINDOWS\\ADCAGENT\\#{server02}"
end
