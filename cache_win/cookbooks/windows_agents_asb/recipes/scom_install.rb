#
# Cookbook Name:: windows_os_asb
# Recipe:: scom_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe SCOM Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "scom_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Check pre-requisites
execute 'start_windows_installer' do
  command "sc start msiserver"
  not_if 'sc query msiserver | find "RUNNING"'
end

#copy files from repository
powershell_script "copy_installers" do
 code <<-EOH
   Copy-Item -Path Z:\\Agentes\\SCOM\\* -Destination C:\\tmp -Recurse
   EOH
   not_if "if((Get-service | where-object {$_.Name -eq 'HealthService'} | measure-object).count -gt 0){ $true } else { $false }"
end

#Install SCOM Agent
execute "install_scom_agent" do
  cwd "C:\\tmp"
  command <<-EOF
  msiexec.exe /i MOMAgent.msi USE_SETTINGS_FROM_AD=0 MANAGEMENT_GROUP=BCPMGSCOM2K12 MANAGEMENT_SERVER_DNS=pscom2k12rmsp02.credito.bcp.com.pe MANAGEMENT_SERVER_AD_NAME=pscom2k12rmsp02.credito.bcp.com.pe ACTIONS_USE_COMPUTER_ACCOUNT=1 USE_MANUALLY_SPECIFIED_SETTINGS=1 AcceptEndUserLicenseAgreement=1 /l*v C:\\tmp\\MOMAgentinstall.log /qn
  EOF
  not_if "sc query healthservice"
end

#Install Certificates
powershell_script "install_scom_certificates" do
  cwd "C:\\tmp"
  code <<-EOH
  Certutil -addstore -f "TrustedPublisher" NewChain
  Certutil -addstore -f "CA" newCARoot.cer
  EOH
  not_if "if((Get-service | where-object {$_.Name -eq 'HealthService'} | measure-object).count -gt 0){ $true } else { $false }"
end

#Check if healthservice is running and enabled
windows_service "healthservice" do
  action :start
end
