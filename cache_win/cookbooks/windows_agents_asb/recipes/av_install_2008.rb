#
# Cookbook Name:: windows_agents_asb
# Recipe:: av_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

log "Antivirus Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "av_install",Chef.run_context.node.run_list
    )
  end
end

#include_recipe "bcp_common::tmp_directory"
#include_recipe "bcp_common::windows_share"

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip

batch 'obtener_ambiente' do
code <<-EOH
set amb=%computername%
set amb=%amb:~-3%
set amb=%amb:~0,1%
EOH
end

case amb
	when 'P'
		batch 'copiar_archivos' do
		code <<-EOH
		copy \\\\padminicop01\\Repositorio_ICO\\Agentes\\McAfee\\Produccion\\*.* C:\\tmp
		EOH
		end
	when 'D'
		batch 'copiar_archivos' do
		code <<-EOH
		copy \\\\padminicop01\\Repositorio_ICO\\Agentes\\McAfee\\Desa_Cert\\*.* C:\\tmp
		EOH
		end
end 
