#
# Cookbook Name:: windows_agents_asb
# Recipe:: av_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

log "Antivirus Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "av_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip


case organizacion
  when 'BCP'
	case ambiente
		when 'Produccion'
			#Copy files from repository
			powershell_script "copy_installers" do
			  code <<-EOH
			  Copy-Item -Path Z:\\Agentes\\McAfee\\Produccion\\* -Destination C:\\tmp -Recurse
			  EOH
			  not_if "Test-Path C:\\tmp\\Agente\\FramePkg.exe"
			end
			#Install McAfee Agent
			batch "agent_installation" do
			  code <<-EOH
				"C:\\tmp\\Agente\\FramePkg.exe" /install=agent /SILENT /forceinstall
				"C:\\program files (x86)\\Mcafee\\Common Framework\\CmdAgent.exe" /p
				"C:\\program files (x86)\\Mcafee\\Common Framework\\CmdAgent.exe" /e
				EOH
				not_if "cd C:\\program files (x86)\\Mcafee"
			end
			case os
				when 'Microsoft Windows Server 2016 Standard'
					#Install McAfee Antivirus
					batch "av_installation" do
					  code <<-EOH
					  "C:\\tmp\\Antivirus\\W2K16\\SetupVSE.exe" /q REBOOT=R ADDLOCAL=ALL
					  "C:\\program files (x86)\\Mcafee\\VirusScan Enterprise\\mcupdate.exe" /update /quiet
					   EOH
					  not_if "sc query mcshield"
					end
				else
					#Install mc Afee Antivirus
					batch "av_installation" do
					  code <<-EOH
					  "C:\\tmp\\Antivirus\\SetupVSE.exe" /q REBOOT=R ADDLOCAL=ALL
						"C:\\program files (x86)\\Mcafee\\VirusScan Enterprise\\mcupdate.exe" /update /quiet
					   EOH
					  not_if "sc query mcshield"
					end
			end
		else
			#Copy files from repository
			powershell_script "copy_installers" do
			 code <<-EOH
			   Copy-Item -Path Z:\\Agentes\\McAfee\\Desa_Cert\\* -Destination C:\\tmp -Recurse
			   EOH
			   not_if "Test-Path C:\\tmp\\Agente\\FramePkg.exe"
			end
			#Install McAfee Agent
			batch "agent_installation" do
			  code <<-EOH
				"C:\\tmp\\Agente\\FramePkg.exe" /install=agent /SILENT /forceinstall
				"C:\\program files (x86)\\Mcafee\\Common Framework\\CmdAgent.exe" /p
				"C:\\program files (x86)\\Mcafee\\Common Framework\\CmdAgent.exe" /e
				EOH
				not_if "cd C:\\program files (x86)\\Mcafee"
			end
			case os
				when 'Microsoft Windows Server 2016 Standard'
					#Install McAfee Antivirus
					batch "av_installation" do
					  code <<-EOH
					  "C:\\tmp\\Antivirus\\W2K16\\SetupVSE.exe" /q REBOOT=R ADDLOCAL=ALL
					  "C:\\program files (x86)\\Mcafee\\VirusScan Enterprise\\mcupdate.exe" /update /quiet
					   EOH
					  not_if "sc query mcshield"
					end
				else
					#Install mc Afee Antivirus
					batch "av_installation" do
					  code <<-EOH
					  "C:\\tmp\\Antivirus\\SetupVSE.exe" /q REBOOT=R ADDLOCAL=ALL
						"C:\\program files (x86)\\Mcafee\\VirusScan Enterprise\\mcupdate.exe" /update /quiet
					   EOH
					  not_if "sc query mcshield"
					end
			end
	end
  when 'PRIMA'
  when 'CAPITAL'
end 
  