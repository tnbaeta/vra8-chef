#
# Cookbook Name:: windows_os_asb
# Recipe:: chauditor_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe Change Auditor Install" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "chauditor_install",Chef.run_context.node.run_list
    )
  end
end

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Check pre-requisites
execute 'start_windows_installer' do
  command "sc start msiserver"
  not_if 'sc query msiserver | find "RUNNING"'
end

powershell_script "copy_installers" do
 code <<-EOH
   Copy-Item -Path Z:\\Agentes\\ChangeAuditor\\* -Destination C:\\tmp -Recurse
   EOH
   not_if "Test-Path C:\\tmp\\NDP452-KB2901907-x86-x64-AllOS-ENU.exe"
end

#Install windows net framework 4
powershell_script "install_netfx40" do
  cwd "C:\\tmp"
  code <<-EOH
   Set-ExecutionPolicy Bypass
   $installer = "C:\\tmp\\NDP452-KB2901907-x86-x64-AllOS-ENU.exe"
   $args = "/q /norestart"
   $process = (Start-Process -FilePath $installer -ArgumentList $args -Wait -Verb RunAs -PassThru)
   if($process.exitcode -eq 0){
     Write-Host "The netfx4 was installed successfully"
   }else{
     Write-Host "The netfx4 was installed"
	 Write-Host $process.exitcode
	 exit 0
   }
  EOH
  not_if "$R = @();$dotNetRegistry  = 'SOFTWARE\\Microsoft\\NET Framework Setup\\NDP';$dotNet4Registry = 'SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full';$dotNet4Builds = @{30319  =  '4.0';378389 = '4.5';378675 = '4.5.1 (8.1/2012R2)';378758 = '4.5.1 (8/7 SP1/Vista SP2)';379893 = '4.5.2';	380042 = '4.5 and later with KB3168275 rollup';393295 = '4.6 (Windows 10)';393297 = '4.6 (NON Windows 10)';394254 = '4.6.1 (Windows 10)';394271 = '4.6.1 (NON Windows 10)';394802 = '4.6.2 (Windows 10 Anniversary Update)';394806 = '4.6.2 (NON Windows 10)'};$Computer = '.';if($regKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $Computer)) {	if ($netRegKey = $regKey.OpenSubKey(\"$dotNetRegistry\")) {foreach ($versionKeyName in $netRegKey.GetSubKeyNames()) {if ($versionKeyName -match '^v[123]') {$versionKey = $netRegKey.OpenSubKey($versionKeyName);$version = [version]($versionKey.GetValue('Version', ''));$R+=New-Object -TypeName PSObject -Property @{ComputerName = $Computer;NetFXBuild = $version.Build;NetFXVersion = '' + $version.Major + '.' + $version.Minor} }}};if ($net4RegKey = $regKey.OpenSubKey(\"$dotNet4Registry\")) {if(-not ($net4Release = $net4RegKey.GetValue('Release'))) {$net4Release = 30319};$R+=New-Object -TypeName PSObject -Property @{ComputerName = $Computer;NetFXBuild = $net4Release;NetFXVersion = $dotNet4Builds[$net4Release]}}};if(@($R | Where-Object {$_.NetFXVersion -ge \"4.5.2\"} | Select-Object ComputerName, NetFXVersion, NetFXBuild ).Count -gt 0) { return $true} else { return $false }"
end

#Install Change Auditor msi package
powershell_script "install_change_auditor" do
  cwd "C:\\tmp"
  code <<-EOH
   Set-ExecutionPolicy Bypass
   $installer = "msiexec.exe"
   $args = "/i `"C:\\tmp\\Dell Change Auditor Agent 6 (x64) v6.9.2.msi`" /quiet /qb INSTALLATION_NAME_VALID=1 INSTALLATION_NAME=`"BCP`" REBOOT=`"ReallySuppress`" ALLUSERS=1 STAGINGPATH=`"C:\\Windows\\TEMP\\`""
   $process = (Start-Process -FilePath $installer -ArgumentList $args -Wait -Verb RunAs -PassThru)
   if($process.exitcode -eq 0){
     Write-Host "The Change Auditor msi package installed successfully"
   }else{
     Write-Host "The Change Auditor msi package retuned a non expected exitcode"
	 Write-Host $process.exitcode
	 exit 0
   }
  EOH
  not_if "if ((Get-Service -Name NPSrvHost -ErrorAction SilentlyContinue).Length -gt 0){ return $true } else { return $false }"
end