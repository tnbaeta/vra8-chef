# Cookbook Name:: recursos_globales
# Author: Jorge Copia Silva.
# Formatea los atributos pasados desde Allegro

def load_params(filePath)
    if(!File.exist?(filePath))
        print 'No existe el archivo de parametos: ' + filePath
        return
    end
    line_num=0
    text=File.open(filePath).read
    text.each_line do |line|
        data = line.split(':')
        if data.length > 2
            raise 'Los parametros de Allegro no cumplen el formato estipulado'
        end 
        node.default[data[0].downcase.strip]=data[1].gsub(/\r|\n/, "").downcase.strip
    end
end

load_params('c:\\windows\\Temp\\params.txt');
load_params('c:\\windows\\Temp\\sql_params.txt');

#Inicializa parametro global
node.default["global_helper"] = GlobalEvents::Helper.new