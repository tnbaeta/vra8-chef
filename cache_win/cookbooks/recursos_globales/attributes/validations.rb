version = node['kernel']['os_info']['name'].match /\s(?<version>[0-9]+)\s/
node.default['windows_version'] = version[:version]
node.default["is_bcp"] =  node.default["organizacion"] == 'bcp' ? true : false
node.default["is_w2016"] =  version[:version].to_i == 2016 ? true : false
