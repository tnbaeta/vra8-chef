require 'net/http'

module GlobalEvents
  
  class Helper
    def notify_failure(exception, node)
        emails = ['jcopia@bcp.com.pe', 'joelrodriguezc@bcp.com.pe', 'dportillo@bcp.com.pe']
        subject = "Chef run failed on #{node['hostname']} Date: #{Time.now.rfc2822}"
        message = "Se ha producido un error al ejecutar una receta chef <br>"
        message << "Hostname: #{node['hostname']} - #{node['ipaddress']}.<br>Runlis: #{node.run_list},<br>Error: #{exception}"
        emails.each do | email |
          uri = URI('http://picop01.credito.bcp.com.pe:20000/sendemail')
          params = { :to => "#{email}", :subject => subject, :message => message, :apikey => '0xfwb9030w=m08'  }
          uri.query = URI.encode_www_form(params)
          res = Net::HTTP.get_response(uri)
          puts res.body if res.is_a?(Net::HTTPSuccess)
        end
    end

    def save_log(run_list, message)
      out_file = File.new("c:\\windows\\temp\\chef_global_log.txt", "a+")
      out_file.puts("#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} | #{run_list} | #{message}")
      out_file.close
    end
  
  end
end
