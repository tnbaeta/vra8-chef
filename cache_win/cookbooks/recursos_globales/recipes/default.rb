
Chef.event_handler do
    on :run_failed do |exception|
      GlobalEvents::Helper.new.notify_failure(
        exception.message, Chef.run_context.node
      )
    end
    on :run_completed do
      GlobalEvents::Helper.new.save_log(
        Chef.run_context.node.run_list, "Se termino de ejecutar la receta"
      )
    end
end