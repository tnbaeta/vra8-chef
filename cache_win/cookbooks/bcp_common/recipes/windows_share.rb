#
# Cookbook Name:: bcp_common
# Recipe:: windows_share
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

#Carga de credenciales
my_bag = data_bag_item("recursos","uncpath")
credentials = Chef::EncryptedDataBagItem.load("credentials", "windows","awkbash")
username = credentials["username"]
clave = credentials["password"]

#Montado de unidad
powershell_script "mount_windows_share" do
 code <<-EOH
   $user= "#{username}"
   $domainuser = "BCPDOM\\$user"
   $password = "#{clave}"
   $key = (1..16)
   $my_secure_password = $password | ConvertTo-SecureString -Key $key
   $cred = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
   New-PSDrive -Name "Z" -Root "\\\\PADMINICOP01.credito.bcp.com.pe\\Repositorio_ICO" -PSProvider Filesystem -Credential $cred -Scope "Global" -Persist
   EOH
   not_if "Test-Path Z:"
end