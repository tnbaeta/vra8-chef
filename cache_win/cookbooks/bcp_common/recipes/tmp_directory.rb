#
# Cookbook Name:: bcp_common
# Recipe:: tmp_directory
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

#create a directory tmp
directory "C:\\tmp" do
  action :create
end