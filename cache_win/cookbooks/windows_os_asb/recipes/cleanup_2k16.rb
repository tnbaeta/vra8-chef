#
# Cookbook Name:: windows_os_asb
# Recipe:: cleanup_2k16
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe cleanup 2k16" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "cleanup_2k16",Chef.run_context.node.run_list
    )
  end
end

#Remove default user Admin if exists
user "Admin" do
  action :remove
end

#Remove group Midrange Windows in Power Users Group if exists
group "Power Users" do
  action :modify
  excluded_members ["BCPDOM\\Midrange - Windows"]
  append true
end

#Remove group Midrange Windows in Backup Operators Group if exists
group "Backup Operators" do
  action :modify
  excluded_members ["BCPDOM\\Midrange - Windows"]
  append true
end

#Remove user APSSPRO in Performance Monitor Group if exists
group "Performance Monitor Users" do
  action :modify
  excluded_members ["BCPDOM\\APSSPRO"]
  append true
end

#Remove group Midrange Windows in Remote Desktop Group if exists
group "Remote Desktop Users" do
  action :modify
  excluded_members ["BCPDOM\\Midrange - Windows"]
  append true
end

#Start service DHCP Client
windows_service "Dhcp" do
  action :configure_startup
  startup_type :automatic
  notifies :start, 'windows_service[Dhcp]', :delayed
end

windows_service "Dhcp" do
  action :nothing
end

#GPO Update force to sync policy
execute "gpupdate_force" do
  command "gpupdate /force"
end

#Remove windows share
powershell_script "remove_windows_share" do
 code <<-EOH
   Remove-PSDrive Z
   EOH
   only_if "Test-Path Z:"
end

#Stop SSH Service
powershell_script "stop_ssh_service" do
 code <<-EOH
   Stop-Service sshd
   Set-Service sshd -startupType disabled
   #Remove-Item -Path C:\\tmp -Recurse -Force
   EOH
   only_if "Test-Path C:\\tmp"
end


#Remove user AdminASB if exists
user "AdminASB" do
  action :remove
end

