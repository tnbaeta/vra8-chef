#
# Cookbook Name:: windows_os_asb
# Recipe:: joindomain
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Join to domain BCPDOM" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "joindomain",Chef.run_context.node.run_list
    )
  end
end

#Initialize variable credentials from encripted databag
credentials = Chef::EncryptedDataBagItem.load("credentials", "windows","awkbash")
username = credentials["username"]
clave = credentials["password"]

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")

#1. Organization Filter
case organizacion
  when 'BCP'
    #Declare a powershell resource for join to domain.
	powershell_script "join_windows_domain" do
	  code <<-EOH
	  $domain = "credito.bcp.com.pe"
	  $ou = "OU=BCP - Servidores #{ambiente},OU=BCP - Servidores,DC=credito,DC=bcp,DC=com,DC=pe"
			 $user = "#{username}"
				 $domainuser = "BCPDOM\\$user"
			 $password = "#{clave}"
			 $key=(1..16)
			 $my_secure_password = $password | ConvertTo-SecureString -Key $key
			 $credentials = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
			  try{
			  Add-Computer -DomainName $domain -Credential $credentials -OUPath $ou
			  }catch{
				  Write-Error $error[0]
				  exit 1
			  }
		  EOH
	#Using idempotence in order to not get executed twice
	not_if "(gwmi win32_computersystem).partofdomain"
	end
	
  when 'PRIMA'
    #print "#{node[:organizacion]}"
	#Declare a powershell resource for join to domain.
	powershell_script "join_windows_domain" do
	  code <<-EOH
	  $domain = "domain.primaafp.com.pe"
	  $ou = "OU=PRIMA - Servidores #{ambiente},OU=PRIMA - Servidores,DC=domain,DC=primaafp,DC=com,DC=pe"
			 $user = "#{username}"
				 $domainuser = "BCPDOM\\$user"
			 $password = "#{clave}"
			 $key=(1..16)
			 $my_secure_password = $password | ConvertTo-SecureString -Key $key
			 $credentials = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
			  try{
			  Add-Computer -DomainName $domain -Credential $credentials -OUPath $ou
			  }catch{
				  Write-Error $error[0]
				  exit 1
			  }
		  EOH
	#Using idempotence in order to not get executed twice
	not_if "(gwmi win32_computersystem).partofdomain"
	end
	
  when 'CAPITAL'
    #print "#{node[:organizacion]}"
	#Declare a powershell resource for join to domain.
	powershell_script "join_windows_domain" do
	  code <<-EOH
	  $domain = "capitaldom.local"
	  $ou = "OU=#{ambiente},OU=CAPITAL - Servidores,DC=capitaldom,DC=local"
			 $user = "#{username}"
				 $domainuser = "BCPDOM\\$user"
			 $password = "#{clave}"
			 $key=(1..16)
			 $my_secure_password = $password | ConvertTo-SecureString -Key $key
			 $credentials = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
			  try{
			  Add-Computer -DomainName $domain -Credential $credentials -OUPath $ou
			  }catch{
				  Write-Error $error[0]
				  exit 1
			  }
		  EOH
	#Using idempotence in order to not get executed twice
	not_if "(gwmi win32_computersystem).partofdomain"
  end
end
