#
# Cookbook Name:: windows_os_asb
# Recipe:: pagefile
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Configure pagefile size" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "pagefile",Chef.run_context.node.run_list
    )
  end
end

case node["kernel"]["os_info"]["caption"]
when 'Microsoft Windows Server 2016 Standard'
  powershell_script "creacion_volumen_W2K16" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $disk = Get-Disk | Where-Object Number -eq 1
	  Initialize-Disk $disk.number -PartitionStyle MBR
	  $disk | Set-Disk -IsOffline $False
	  New-Partition -DiskNumber $disk.number -UseMaximumSize -DriveLetter P
	  Format-Volume -DriveLetter P -FileSystem NTFS -Confirm:$False
	  EOH
	  not_if "Test-Path P:\\"
  end
  
  powershell_script "disable_automatic_pagefile" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $computersys = Get-WmiObject Win32_ComputerSystem -EnableAllPrivileges
	  $computersys.AutomaticManagedPagefile = $False
	  $computersys.Put()
	  EOH
	  only_if "(Get-WmiObject Win32_ComputerSystem -EnableAllPrivileges).AutomaticManagedPagefile"
  end

  powershell_script "configure_c_drive_pagefile" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $pagefile = gwmi Win32_PageFileSetting | Where-Object { $_.Name -like "c*pagefile*"}
	  $pagefile.InitialSize = [int]((($physicalmem.capacity*0.5)/1024)/1024)+1024
	  $pagefile.MaximumSize = [int]((($physicalmem.capacity*0.5)/1024)/1024)+1024
	  $pagefile.Put()
	  EOH
	  only_if "if((gwmi Win32_PageFileSetting).MaximumSize -eq 0) { return $true } else {return $false}"
  end

  powershell_script "create_pagefile_on_p_drive" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $physicalmem = Get-WmiObject Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum
	  Set-WMIInstance -class Win32_PageFileSetting -Arguments @{name="P:\\pagefile.sys";InitialSize = [int]((($physicalmem.Sum)/1024)/1024)-1024;MaximumSize =[int]((($physicalmem.Sum)/1024)/1024)-1024}
	  EOH
	  not_if "Test-Path P:\\pagefile.sys"
	  end

  #remove everyone from pagefile disk
  execute "set permission" do
	  command "icacls P:\\ /remove everyone"
	  only_if "cd P:"
  end

  execute "set permission" do
	  command "icacls C:\\ /remove everyone"
	  only_if "cd C:"
  end
else
	case node["kernel"]["os_info"]["caption"]
	  when 'Microsoft Windows Server 2012 R2 Standard'
		powershell_script "creacion_volumen_W2K12" do
		guard_interpreter :powershell_script
		code <<-EOH
		$disk = Get-Disk | Where-Object Number -eq 1
		Initialize-Disk $disk.number -PartitionStyle MBR
		$disk | Set-Disk -IsOffline $False
		New-Partition -DiskNumber $disk.number -UseMaximumSize -DriveLetter P
		Format-Volume -DriveLetter P -FileSystem NTFS -Confirm:$False
		EOH
		not_if "Test-Path P:\\"
	  end
	  else
		powershell_script "creacion de volumen W2K8" do
		guard_interpreter :powershell_script
		code <<-EOH
		"select disk 1`r clean`r create partition primary`r format fs=ntfs unit=65536 quick`r active`r assign letter=P" | diskpart
		EOH
		not_if "Test-Path P:\\"
	  end
	end
	powershell_script "disable_automatic_pagefile" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $computersys = Get-WmiObject Win32_ComputerSystem -EnableAllPrivileges
	  $computersys.AutomaticManagedPagefile = $False
	  $computersys.Put()
	  EOH
	  only_if "(Get-WmiObject Win32_ComputerSystem -EnableAllPrivileges).AutomaticManagedPagefile"
	end

	powershell_script "configure_c_drive_pagefile" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $pagefile = gwmi Win32_PageFileSetting | Where-Object { $_.Name -like "c*pagefile*"}
	  $pagefile.InitialSize = [int]((($physicalmem.capacity*0.5)/1024)/1024)+1024
	  $pagefile.MaximumSize = [int]((($physicalmem.capacity*0.5)/1024)/1024)+1024
	  $pagefile.Put()
	  EOH
	  only_if "if((gwmi Win32_PageFileSetting).MaximumSize -eq 0) { return $true } else {return $false}"
	end

	powershell_script "create_pagefile_on_p_drive" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $physicalmem = Get-WmiObject Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum
	  Set-WMIInstance -class Win32_PageFileSetting -Arguments @{name="P:\\pagefile.sys";InitialSize = [int]((($physicalmem.Sum)/1024)/1024)-1024;MaximumSize =[int]((($physicalmem.Sum)/1024)/1024)-1024}
	  EOH
	  not_if "Test-Path P:\\pagefile.sys"
	end
end
