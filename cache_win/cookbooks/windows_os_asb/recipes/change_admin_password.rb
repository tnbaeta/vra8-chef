
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe change admin password" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "change_admin_password",Chef.run_context.node.run_list
    )
  end
end

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip


#Library to generate randomPassword
#SecureRandom will be used for generate very secure password for best Security
require 'securerandom'
randomPasswordAdministrator = Array.new(12){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join + '$'

#Change Administrator Password
user "Admin#{ambiente}" do
  password "#{randomPasswordAdministrator}"
end

#Send Administrator Password
powershell_script "send_admin_password" do
  guard_interpreter :powershell_script
  code <<-EOH
	$para="jorgeneirac@bcp.com.pe"
	$name=hostname
	$pass="#{randomPasswordAdministrator}"
	$subject="Cambio de password Administrator"
	$message="El password del usuario administrador del servidor $name es: $pass"
	$url="http://picop01.credito.bcp.com.pe:20000/sendemail?&to=$para&subject=$subject&message=$message&apikey=0xfwb9030w=m08"
	Invoke-RestMethod -uri $url
	EOH
end
