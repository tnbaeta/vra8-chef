#
# Cookbook Name:: windows_os_asb
# Recipe:: users_assign
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe users assign" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "Users_Assign",Chef.run_context.node.run_list
    )
  end
end

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip

#Organization Filter
case organizacion
  when 'BCP'
    #Asignación al Grupo Administrators
    group "Administrators" do
	action :modify
	members ["BCPDOM\\Control de Cambios - Server Provisioning", "BCPDOM\\Administracion de Almacenamiento", "BCPDOM\\Monitoring COS Patch Management", "BCPDOM\\SdS - Helpdesk Accesos y Permisos", "BCPDOM\\UGS Infraestr Generic", "BCPDOM\\Innovation"]
	append true
	end
	
	#Asignación al Grupo Power Users
	group "Power Users" do
	action :modify
	members ["BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Cambios - CMDB Admins", "BCPDOM\\Control de Producción", "BCPDOM\\Control Operac - Sistemas Distribuidos", "BCPDOM\\MaD - Windows", "BCPDOM\\Midrange - Windows", "BCPDOM\\Monitoring COS", "BCPDOM\\TaS - Sistemas Distribuidos"]
	append true
	end
	
	#Asignación al Grupo Backup Operators
	group "Backup Operators" do
    action :modify
    members ["BCPDOM\\APTSMPRO", "BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción", "BCPDOM\\Control Operac - Sistemas Distribuidos", "BCPDOM\\MaD - Windows",
    "BCPDOM\\Midrange - Windows"]
    append true
    end
	
	#Asignación al Grupo Performance Monitor Users
	group "Performance Monitor Users" do
    action :modify
    members ["BCPDOM\\APSSPRO", "BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción"]
    append true
    end
	
	#Asignación al Grupo Performance Log Users
	group "Performance Log Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción"]
    append true
    end

	#Asignación al Grupo Remote Desktop Users
	group "Remote Desktop Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción", "BCPDOM\\Control de Cambios - CMDB Admins",
    "BCPDOM\\MaD - Windows", "BCPDOM\\Midrange - Windows", "BCPDOM\\Monitoring COS", "BCPDOM\\TaS - Sistemas Distribuidos",
    "BCPDOM\\Control Operac - Sistemas Distribuidos"]
    append true
	end
	
  	case ambiente
	  when 'Produccion'
		  #Asignación al Grupo Administrators
		  group "Administrators" do
		  action :modify
		  members ["BCPDOM\\Control de Cambios - Release", "BCPDOM\\Midrange - Windows Prod Admin"]
		  append true
		  end 
		  
	  when 'Certificacion'
		  #Asignación al Grupo Administrators
		  group "Administrators" do
		  action :modify
		  members ["BCPDOM\\Midrange - Windows CertDesa Admins", "BCPDOM\\Control de Cambios - Congelamientos en Certificacion"]
		  append true
		  end
		  
		  #Asignación al Grupo Performance Monitor Users
  		  group "Performance Monitor Users" do
		  action :modify
		  members ["BCPDOM\\CdS - Equipo Estres"]
		  append true
		  end
	  
	  when 'Desarrollo'
		  #Asignación al Grupo Administrators
		  group "Administrators" do
		  action :modify
		  members ["BCPDOM\\Control de Cambios - Release", "BCPDOM\\Midrange - Windows CertDesa Admins"]
		  append true
		  end
	end
	case tipo
	  when 'Aplicacion'
		  #Asignación al Grupo Administrators
		  group "Administrators" do
	      action :modify
	      members ["BCPDOM\\SDI - Windows"]
		  append true
		  end
	  else
		  #Asignación al Grupo Administrators
	      group "Administrators" do
	      action :modify
	      members ["BCPDOM\\Network Security - Windows"]
		  append true
		  end
    end
	  
  when 'PRIMA'
    include_recipe "windows_os_asb::prima_groups_base"
    case ambiente
		when 'Produccion'
		  case os
			when 'Microsoft Windows Server 2012 R2 Standard'
			  group "Administrators" do
				action :modify
				members ["BCPDOM\\Midrange - Windows Prod Admin", "BCPDOM\\Control de Cambios - Release"]
				append true
			  end
			when 'Microsoft Windows Server 2008 R2 Standard'
			  group "Administrators" do
				action :modify
				members ["BCPDOM\\Midrange - Windows Prod Admin","BCPDOM\\OM Admins","BCPDOM\\Control de Cambios - Release"]
				append true
			  end
		  end
		when 'Certificacion'
		  group "Administrators" do
			action :modify
			members ["BCPDOM\\Midrange - Windows CertDesa Admins","BCPDOM\\Control de Cambios - Congelamientos en Certificacion"]
			append true
		  end
		when 'Desarrollo'
		  group "Administrators" do
			action :modify
			members ["BCPDOM\\Midrange - Windows CertDesa Admins"]
			append true
		  end
	end
  when 'CAPITAL'
    include_recipe "windows_os_asb::capital_groups_base"
	case ambiente
		when 'Produccion'
		  case os
			when 'Microsoft Windows Server 2008 R2 Standard'
			 group "Administrators" do
			 action :modify
			 members ["BCPDOM\\Midrange - Windows Prod Admin","BCPDOM\\Control de Cambios - Release"]
			 append true
			 end
			 
			else
			  group "Administrators" do
			  action :modify
			  members ["BCPDOM\\Midrange - Windows Prod Admin","BCPDOM\\Control de Cambios - Release"]
			  append true
			  end
			
			case tipo
			  when 'Aplicacion'
				group "Administrators" do
				action :modify
				members ["BCPDOM\\SDI - Windows"]
				append true
				end
			else
				group "Administrators" do
				action :modify
				members ["BCPDOM\\Seguridad Credicorp"]
				append true
				end
			end
		  end
	
		when 'Certificacion'
		  case os
		    when 'Microsoft Windows Server 2008 R2 Standard'
			 group "Administrators" do
			 action :modify
			 members ["BCPDOM\\Midrange - Windows CertDesa Admins","BCPDOM\\Control de Cambios - Congelamientos en Certificacion"]
			 append true
			 end
			 
			else
			  group "Administrators" do
			  action :modify
			  members ["BCPDOM\\Midrange - Windows CertDesa Admins","BCPDOM\\Control de Cambios - Congelamientos en Certificacion"]
			  append true
			  end
			
			case tipo
			  when 'Aplicacion'
				group "Administrators" do
				action :modify
				members ["BCPDOM\\SDI - Windows"]
				append true
				end
			  
			  else
				group "Administrators" do
				action :modify
				members ["BCPDOM\\Seguridad Credicorp"]
				append true
				end
			end
		  end
		  
		when 'Desarrollo'
		  case os
		    when 'Microsoft Windows Server 2008 R2 Standard'
			 group "Administrators" do
			 action :modify
			 members ["BCPDOM\\Midrange - Windows CertDesa Admins"]
			 append true
			 end
			 
			else
			  group "Administrators" do
			  action :modify
			  members ["BCPDOM\\Midrange - Windows CertDesa Admins"]
			  append true
			  end
			
			case tipo
			  when 'Aplicacion'
				group "Administrators" do
				action :modify
				members ["BCPDOM\\SDI - Windows"]
				append true
				end
			else
				group "Administrators" do
				action :modify
				members ["BCPDOM\\Seguridad Credicorp"]
				append true
				end
			end
		  end
	end
 end