#
# Cookbook Name:: windows_os_asb
# Recipe:: Open_Ports
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

log "Open Ports" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "Open_Ports",Chef.run_context.node.run_list
    )
  end
end

os = node['kernel']['os_info']['caption'].strip

case os
  when 'Microsoft Windows Server 2016 Standard'
  powershell_script "open_firewall_ports" do
      guard_interpreter :powershell_script
      code <<-EOH
      Set-NetFirewallRule -Name "FPS-ICMP4-ERQ-In" -Enabled true
	  Set-NetFirewallRule -Name "FPS-LLMNR-In-UDP" -Enabled true
	  Set-NetFirewallRule -Name "NETDIS-LLMNR-In-UDP" -Enabled true
	  Set-NetFirewallRule -Name "RemoteDesktop-UserMode-In-TCP" -Enabled true
      EOH
  end
end