#
# Cookbook Name:: windows_os_asb
# Recipe:: activate_windows
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe activate windows" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "Activate_Windows",Chef.run_context.node.run_list
    )
  end
end

#Initialize variable credentials from encripted databag
credentials = Chef::EncryptedDataBagItem.load("credentials", "windows","awkbash")
username = credentials["username"]
clave = credentials["password"]
os = node['kernel']['os_info']['caption'].strip

#Create logs directory 
directory "C:\\logs" do
  action :create
  not_if "Test-Path C:\\logs"
end

#Activate windows licence
case os
  when 'Microsoft Windows Server 2008 R2 Standard'
  powershell_script "activate_windows_2008" do
      guard_interpreter :powershell_script
      code <<-EOH
      netsh winhttp set proxy pfftmgp01.credito.bcp.com.pe:80
      cscript C:\\windows\\System32\\slmgr.vbs /ipk YC6KT-GKW9T-YTKYR-T4X34-R7VHC
      cscript C:\\windows\\System32\\slmgr.vbs /ato
      netsh winhttp reset proxy
      EOH
      not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
  end
  else
  #enable proxy
  registry_key "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings" do
  values [{
  :name => 'ProxyEnable',
  :type => :dword,
  :data => 1
  }]
  action :create
  not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
  end

  #set proxy
  execute "set proxy" do
  #por el momento se cambia de balanceador balanceadorws.credito.bcp.com.pe:80
  command "netsh winhttp set proxy balanceadorws.credito.bcp.com.pe:80 >> c:\\Logs\\ActivateWindows.txt"
  not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
  end
  
    #set license key 
	case os
	  when 'Microsoft Windows Server 2012 R2 Standard'
		#set license key  
		execute "set license key" do
		command "cscript C:\\windows\\System32\\slmgr.vbs /ipk YN8GT-BKH9Q-V88TJ-TVW7Q-DV4R8 >> c:\\Logs\\ActivateWindows.txt"
		not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
		end
	  when 'Microsoft Windows Server 2016 Standard'
		#set license key  
    #9KQNW-Y27T6-9B8G8-8JCT9-6CRBC
    execute "set license key" do
    command "cscript C:\\windows\\System32\\slmgr.vbs /ipk  WMFNK-DHQ36-M2C32-33BYD-XHKQ2 >> c:\\Logs\\ActivateWindows.txt"
		not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
		end  
	end
	
  #show proxy settings  
  execute "show proxy settings" do
  command "netsh winhttp show proxy >> c:\\Logs\\ActivateWindows.txt"
  not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
  end
  
  #connect to internet  
  powershell_script "activate_windows" do
      guard_interpreter :powershell_script
      code <<-EOH
      $user = "#{username}"
      $domainuser = "BCPDOM\\$user"
      $password = "#{clave}"
      $key=(1..16)
      $my_secure_password = $password | ConvertTo-SecureString -Key $key
      $credential = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
      $de = [ADSI]"WinNT://$env:computername/Administrators,group"
      $de.Add("WinNT://BCPDOM/APICOPRO")
      Start-process -Filepath Powershell -credential $credential -ArgumentList "-command","cscript //B C:\\windows\\System32\\slmgr.vbs /ato" -wait -NoNewWindow
      $de.remove("WinNT://BCPDOM/APICOPRO")
      #if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ exit 0 } else { exit 1 }
      EOH
      not_if "if((gwmi SoftwareLicensingProduct | Where-Object {$_.ApplicationID -eq '55c92734-d682-4d71-983e-d6ec3f16059f' -and $_.LicenseStatus -eq 1} | measure-object).count -gt 0){ $true } else { $false }"
  end
end

#Reset proxy websense / MS TMG
execute "reset proxy" do
  command "netsh winhttp reset proxy"
end

#Evenlog
 log "End recipe activate windows" do
   level :info
 end