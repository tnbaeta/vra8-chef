#
# Cookbook Name:: prima_base
# Recipe:: add_groups_base
#
# Copyright 2016, IBM
#
# All rights reserved - Do Not Redistribute
#

#Get os
os = node['kernel']['os_info']['caption'].strip
#case
case os
when 'Microsoft Windows Server 2012 R2 Standard'

  group "Administrators" do
    action :modify
    members ["BCPDOM\\Control de Cambios - Server Provisioning", "BCPDOM\\SDI - Windows","BCPDOM\\Seguridad Credicorp",
      "BCPDOM\\SdS - Helpdesk Accesos y Permisos","BCPDOM\\UGS Infraestr Generic",
      "BCPDOM\\Administracion de Almacenamiento","BCPDOM\\Monitoring COS Patch Management","BCPDOM\\Innovation"]
    append true
  end

  group "Users" do
    action :modify
    members ["BCPDOM\\Domain Users", "NT AUTHORITY\\Authenticated Users",
       "NT AUTHORITY\\Interactive"]
    append true
  end

  group "Power Users" do
    action :modify
    members ["BCPDOM\\TaS - Sistemas Distribuidos","BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Control de Cambios - CMDB Admins","BCPDOM\\Control de Produccion","BCPDOM\\Monitoring COS",
      "BCPDOM\\Control Operac - Sistemas Distribuidos","BCPDOM\\Midrange - Windows","BCPDOM\\MaD - Windows"]
    append true
  end

  group "Backup Operators" do
    action :modify
    members ["BCPDOM\\APTSMPRO","BCPDOM\\Control de Produccion","BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Control Operac - Sistemas Distribuidos","BCPDOM\\Midrange - Windows","BCPDOM\\MaD - Windows"]
    append true
  end

  group "Performance Monitor Users" do
    action :modify
    members ["BCPDOM\\Control de Produccion","BCPDOM\\Centro de Monitoreo Canales","BCPDOM\\APSSPRO"]
    append true
  end

  group "Performance Log Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción"]
    append true
  end

  group "Remote Desktop Users" do
    action :modify
    members ["BCPDOM\\TaS - Sistemas Distribuidos","BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Control de Cambios - CMDB Admins","BCPDOM\\Control de Produccion",
      "BCPDOM\\Monitoring COS","BCPDOM\\Control Operac - Sistemas Distribuidos",
      "BCPDOM\\Midrange - Windows","BCPDOM\\MaD - Windows", "BCPDOM\\Seguridad Credicorp"]
    append true
  end

  group "Users" do
    action :modify
    excluded_members ["PRIMAAFP\\Domain Users"]
    append true
  end

#check for windows 2008 R2 Servers LBS
when 'Microsoft Windows Server 2008 R2 Standard'
  group "Administrators" do
    action :modify
    members ["BCPDOM\\SdS - Helpdesk Accesos y Permisos","BCPDOM\\Seguridad Credicorp",
      "BCPDOM\\UGS Infraestr Generic","BCPDOM\\Administracion de Almacenamiento", "BCPDOM\\Monitoring COS Patch Management"]
    append true
  end

  group "Users" do
    action :modify
    members ["BCPDOM\\Domain Users", "NT AUTHORITY\\Authenticated Users",
       "NT AUTHORITY\\Interactive", "BCPDOM\\AIO ETI - Consulta TI"]
    append true
  end

  group "Power Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Control de Cambios - CMDB Admins","BCPDOM\\Control de Producción","BCPDOM\\Monitoring COS",
      "BCPDOM\\Midrange - Windows"]
    append true
  end

  group "Backup Operators" do
    action :modify
    members ["BCPDOM\\APTSMPRO","BCPDOM\\Control de Producción","BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Midrange - Windows"]
    append true
  end

  group "Performance Monitor Users" do
    action :modify
    members ["BCPDOM\\APSSPRO","BCPDOM\\Control de Producción","BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\AIO ETI - Consulta TI"]
    append true
  end

  group "Performance Log Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales", "BCPDOM\\Control de Producción", "BCPDOM\\AIO ETI - Consulta TI"]
    append true
  end

  group "Remote Desktop Users" do
    action :modify
    members ["BCPDOM\\Centro de Monitoreo Canales",
      "BCPDOM\\Control de Cambios - CMDB Admins","BCPDOM\\Control de Producción",
      "BCPDOM\\Monitoring COS","BCPDOM\\Midrange - Windows","BCPDOM\\AIO ETI - Consulta TI"]
    append true
  end

  group "Users" do
    action :modify
    excluded_members ["PRIMAAFP\\Domain Users"]
    append true
  end

end
