
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe BCP LBS" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "bcp_lbs",Chef.run_context.node.run_list
    )
  end
end

#Library to generate randomPassword
#SecureRandom will be used for generate very secure password for best Security
require 'securerandom'
randomPasswordFake = Array.new(12){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join + '$'

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip

my_bag = data_bag_item("recursos","uncpath")
credentials = Chef::EncryptedDataBagItem.load("credentials", "windows","awkbash")
username = credentials["username"]
clave = credentials["password"]

#Create directory tmp
directory "C:\\tmp" do
  action :create
 end

#Monta unidad remota
powershell_script "mount_windows_share" do
 code <<-EOH
   $user= "#{username}"
   $domainuser = "BCPDOM\\$user"
   $password = "#{clave}"
   $key = (1..16)
   $my_secure_password = $password | ConvertTo-SecureString -Key $key
   $cred = New-Object -Typename System.Management.Automation.PSCredential -ArgumentList $domainuser,$my_secure_password
   New-PSDrive -Name "Z" -Root "\\\\PADMINICOP01.credito.bcp.com.pe\\Repositorio_ICO" -PSProvider Filesystem -Credential $cred -Scope "Global" -Persist
   EOH
   not_if "Test-Path Z:"
end

#Configure Administrator
powershell_script "set_administrator_#{ambiente}" do
  guard_interpreter :powershell_script
  code <<-EOH
  $u = [adsi]"WinNT://./Administrator,user"
  $u.psbase.rename("Admin#{ambiente}")
  $u.Description=""
  $u.SetInfo()
  $u.commitChanges()
  EOH
  only_if "if((net localgroup Administrators | Where-Object {$_ -eq 'Administrator'} | measure-object).count -eq 1){ $true } else { $false }"
end

#Configure Administrator flags
powershell_script "set_administrator_flags" do
  guard_interpreter :powershell_script
  code <<-EOH
  $os = (Get-WmiObject -class Win32_OperatingSystem).Caption
  $u = [adsi]"WinNT://$env:computername/Admin#{ambiente},user"
  if($os -like "*2016*"){
	$flag = 65536
	$u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
	$u.commitChanges()
  }
  if($os -like "*2012 R2*"){
	$flag = 65536
	$u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
	$u.commitChanges()
  }
  if($os -like "*2008 R2*"){
	$flag = 65600
	$u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
	$u.commitChanges()
  }
  EOH
  not_if "if((net localgroup Administrators | Where-Object {$_ -eq 'AdminDesarrollo'} | measure-object).count -eq 1){ $true } else { $false }"
end

#Rename Guest user
case ambiente
  when 'Desarrollo'
	powershell_script "rename_disable_guest_user" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $guest=[adsi]"WinNT://./Guest,user"
	  $guest.psbase.rename("InvitadoDesa")
	  EOH
	  only_if "if((net localgroup Guests | Where-Object {$_ -eq 'Guest'} | measure-object).count -eq 1){ $true } else { $false }"
	end
  when 'Certificacion'
	powershell_script "rename_disable_guest_user" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $guest=[adsi]"WinNT://./Guest,user"
	  $guest.psbase.rename("InvitadoCert")
	  EOH
	  only_if "if((net localgroup Guests | Where-Object {$_ -eq 'Guest'} | measure-object).count -eq 1){ $true } else { $false }"
	end
  when 'Produccion'
	powershell_script "rename_disable_guest_user" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $guest=[adsi]"WinNT://./Guest,user"
	  $guest.psbase.rename("InvitadoProd")
	  EOH
	  only_if "if((net localgroup Guests | Where-Object {$_ -eq 'Guest'} | measure-object).count -eq 1){ $true } else { $false }"
	end
end

#Remove Domain Admins 
group "Administrators" do
  action :modify
  excluded_members ["BCPDOM\\Domain Admins"]
  append true
end

#Disable SNMPTRAP Service
service "SNMPTRAP" do
  action [:stop, :disable]
end

#Disable Spooler Service
service "Spooler" do
  action [:stop, :disable]
end

#Create a fake Administrator user with complex password
user "Administrator" do
  action :create
  password "#{randomPasswordFake}"
end

#Set cant change password and decription to user Administrator
powershell_script "fake_administrator_password_policy" do
  guard_interpreter :powershell_script
  code <<-EOH
  $flag = 66112
  $u = [adsi]"WinNT://$env:computername/Administrator,user"
  $u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
  $u.Description="Built-in account for administering the computer/domain"
  $u.SetInfo()
  $u.commitChanges()
  EOH
  only_if "if((net localgroup Users | Where-Object {$_ -eq 'Administrator'} | measure-object).count -eq 1){ $true } else { $false }"
end

#Remove fake user Administrator from Users group
group "Users" do
  append true
  excluded_members ["Administrator"]
  action :modify
end

#remove everyone from pagefile disk
execute "set permision" do
  command "icacls P:\\ /remove everyone"
  only_if "cd P:"
end

#Configure EventLog
case os
  when 'Microsoft Windows Server 2016 Standard'
	batch 'Set Log Size' do
	  code <<-EOH
		wevtutil sl Application /ms:104857600
		wevtutil sl Security /ms:201326592
		wevtutil sl Setup /ms:33554432
		wevtutil sl System /ms:104857600
		wevtutil sl Microsoft-Windows-CertificateServicesClient-Lifecycle-System/Operational /ms:104857600
		wevtutil sl Microsoft-Windows-TerminalServices-LocalSessionManager/Operational /ms:104857600
		wevtutil sl Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational /ms:104857600
		EOH
	end
  else
	powershell_script "eventlog_policy" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  Limit-EventLog -LogName System -MaximumSize 100MB -OverflowAction OverwriteAsNeeded
	  Limit-EventLog -LogName Application -MaximumSize 100MB -OverflowAction OverwriteAsNeeded
	  Limit-EventLog -LogName Security -MaximumSize 100MB -OverflowAction OverwriteAsNeeded
	  Limit-EventLog -LogName HardwareEvents -MaximumSize 100MB -OverflowAction OverwriteAsNeeded
	  Limit-EventLog -LogName 'Internet Explorer' -MaximumSize 100MB -OverflowAction OverwriteAsNeeded
	  EOH
	  not_if "if((Get-EventLog -list | where-object {($_.MaximumKilobytes -eq 102400) -and ($_.OverFlowAction -eq 'OverwriteAsNeeded') } | Measure-object).Count -eq 5){$true}else{$false}"
	end
end

#Local Security Policy Settings
case os 
  when 'Microsoft Windows Server 2016 Standard'
	#Network Security: Configure encryption types allowed for Kerberos
	registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System\\Kerberos\\Parameters" do
	  values [{
	  :name => 'SupportedEncryptionTypes',
	  :type => :dword,
	  :data => 2147483644
	  }]
	  recursive true
	  action :create
	end
	
	#UAC: Admin approval Mode for Built-in
	registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
	  values [{
	  :name => 'FilterAdministratorToken',
	  :type => :dword,
	  :data => 1
	  }]
	  action :create
	end

	#Disable IPv6
	powershell_script "Disable_IPv6" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  $NetAdapter = Get-NetAdapterBinding -DisplayName "Internet Protocol Version 6 (TCP/IPv6)"
	  Disable-NetAdapterBinding -Name $NetAdapter.Name -ComponentID ms_tcpip6
	  EOH
	end

	#Remove Windows Features SMB1,TFTP-Client,SNMP-Service
	powershell_script "Remove_Features" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  Remove-WindowsFeature FS-SMB1
	  Remove-WindowsFeature TFTP-Client
	  Remove-WindowsFeature SNMP-Service
	  EOH
	end

	#RDP Settings
	powershell_script "Set_RDP_Service" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'").SetSecurityLayer(1)
	  (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0)
	  (Get-WmiObject -Class Win32_TSClientSetting -Namespace root\\cimv2\\TerminalServices -filter "TerminalName='RDP-tcp'" ).SetMaxMonitors(3)
	  $settings=Get-WmiObject -class Win32_TSClientSetting -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'"
	  $settings.SetClientProperty("Drivemapping",1)
	  $settings.SetClientProperty("AudioCaptureRedir",1)
	  $settings.SetClientProperty("PNPRedirection",1)
	  $settings.ConnectionPolicy=0
	  $settings.DefaultToClientPrinter=0
	  $settings.put()
	  EOH
	end

	#Activate Default to Client Printer RDP Client Setting 
	registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Terminal Server\\WinStations\\RDP-Tcp" do
	  values [{
	  :name => 'fForceClientLptDef',
	  :type => :dword,
	  :data => 0
	  }]
	  action :create
	end

	#Set Processor Scheduling 
	registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\PriorityControl" do
	  values [{
	  :name => 'Win32PrioritySeparation',
	  :type => :dword,
	  :data => 18
	  }]
	  action :create
	end

	#Disable Internal Firewall
	powershell_script "Disable_Services" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
	  EOH
	end
	
	#Disable insecure SSL Protocols and Ciphers [Deshabilita]
	execute "SSL_Best_Practice" do
	  command "Z:\\Util\\IISCryptoCli40.exe /best"
    only_if "cd Z:"
	end
  else
	#Configure RDP Service
	powershell_script "RDP_Properties" do
	  guard_interpreter :powershell_script
	  code <<-EOH
	  (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'").SetSecurityLayer(1)
	  $settings=gwmi -class Win32_TSClientSetting -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'"
	  $settings.SetClientProperty("Drivemapping",1)
	  $settings.SetClientProperty("AudioCaptureRedir",1)
	  $settings.SetClientProperty("PNPRedirection",1)
	  $settings.ConnectionPolicy=0
	  $settings.put()
	  EOH
	end

	registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\lanmanserver\\Parameters" do
	  values [{
	  :name => 'restrictnullsessaccess',
	  :type => :dword,
	  :data => 1
	  }]
	  action :create
	end

	registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\lanmanserver\\Parameters" do
	  values [{
		:name => "NullSessionPipes",
		:type => :multi_string,
		:data => ['']
		}]
		action :create
	end

	#Disable insecure SSL Protocols and Ciphers
	execute "SSL_Best_Practice" do
	  command "Z:\\Util\\IISCryptoCli.exe /best"
	  only_if "cd Z:"
	end

end

#14. Configuracion y aseguramiento del protocolo SSL/ TLS
#powershell_script "Des-Habilita TLS" do
#	guard_interpreter :powershell_script
#	code <<-EOH
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server" -Name "Enabled" -Value 0 
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server" -Name "DisabledByDefault" -Value 1
#		
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server" -Name "Enabled" -Value 0 
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server" -Name "DisabledByDefault" -Value 1
#
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server" -Name "Enabled" -Value 1 
#		set-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server" -Name "DisabledByDefault" -Value 0
#	EOH
#end


#Desactivar PCT1.0
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\PCT 1.0\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\PCT 1.0\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

#Desactivar SSLv2
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 2.0\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 2.0\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

#Desactivar SSLv3
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 3.0\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 3.0\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

#Activar TLS1.0
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.0\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.0\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

#Activar TLS1.1
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.1\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.1\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
  recursive true
  action :create_if_missing
end

#Activar TLS1.2
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.2\\Server\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 00000001},
  {:name => 'DisabledByDefault', :type => :dword, :data => 00000000}]
  recursive true
  action :create_if_missing
end

registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.2\\Client\\" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 1},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create_if_missing
end

#Aseguramiento de llaves del Regestry
#Allow ICMP redirects
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\Tcpip\\Parameters" do
  values [{
  :name => 'EnableICMPRedirect',
  :type => :dword,
  :data => 0
  }]
  action :create
end

#Network Security: LAN Manager Authentication Level
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Lsa" do
  values [{
  :name => 'LmCompatibilityLevel',
  :type => :dword,
  :data => 5
  }]
  action :create
end

#Enable Automatic Logon
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon" do
  values [{
  :name => 'AutoAdminLogon',
  :type => :dword,
  :data => 0
  }]
  action :create
end
