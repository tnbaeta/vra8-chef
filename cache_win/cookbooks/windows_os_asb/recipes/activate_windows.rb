
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe Windows Cleanup" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "windows_activate",Chef.run_context.node.run_list
    )
  end
end

#Limpieza de variables globales
organizacion=node['organizacion'].gsub(/\s+/, "")
ambiente=node['ambiente'].gsub(/\s+/, "")
tipo=node['tipo'].gsub(/\s+/, "")
os = node['kernel']['os_info']['caption'].strip

case os 
  when 'Microsoft Windows Server 2012 R2 Standard'
  include_recipe "windows_os_asb::force_activate"
  else
  include_recipe "windows_os_asb::reactivate_windows"
end