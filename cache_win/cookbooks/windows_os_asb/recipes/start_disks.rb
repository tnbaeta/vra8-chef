#
# Cookbook Name:: windows_os_asb
# Recipe:: start_disks
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Configure and inicialize disks" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "start_disks",Chef.run_context.node.run_list
    )
  end
end

case node["kernel"]["os_info"]["caption"]
  when 'Microsoft Windows Server 2012 R2 Standard'
    powershell_script "creacion_volumen_W2K12" do
    guard_interpreter :powershell_script
    code <<-EOH
    $volumes = get-content c:\\tmp\\disks.txt
    $letters = $volumes.split("{,}")
    $disks = Get-Disk | Where-object {$_.NumberOfPartitions -eq 0}
    $index = 0
    foreach ($disk in $disks) {
      if ($disk.number -gt 1) {
	    $index = $disk.number - 2
      Initialize-Disk $disk.number -PartitionStyle MBR
      $disk | Set-Disk -IsOffline $False
      New-Partition -DiskNumber $disk.number -UseMaximumSize -DriveLetter $letters[$index]
      Format-Volume -DriveLetter $letters[$index] -FileSystem NTFS -Confirm:$False
      }
    }
    EOH
    only_if "Test-Path c:\\tmp\\disks.txt"
  end
  else
    powershell_script "creacion de volumen W2K8" do
    guard_interpreter :powershell_script
    code <<-EOH
    $volumes = get-content c:\\tmp\\disks.txt
    $letters = $volumes.split("{,}")
    if($letters){
      foreach($disk in get-wmiobject Win32_DiskDrive -Filter "Partitions = 0"){
      $disk.DeviceID
      $disk.Index
      "select disk "+$disk.Index+"`r clean`r create partition primary`r format fs=ntfs unit=65536 quick`r active`r assign letter="+$letters[$disk.Index - 2] | diskpart
      }
    }
    EOH
    only_if "Test-Path c:\\tmp\\disks.txt"
  end
end

powershell_script "Local_Log01" do
  code <<-EOH
  echo "$(Get-Date) Se configura el Pagefile del servidor" >> c:\logs\trace.log
  EOH
  only_if "Test-Path c:\\tmp\\disks.txt"
end