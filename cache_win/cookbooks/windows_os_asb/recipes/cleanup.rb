#
# Cookbook Name:: windows_os_asb
# Recipe:: cleanup
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

log "Begin recipe cleanup" do
  level :info
end

Chef.event_handler do
  on :run_failed do |exception|
    HandlerSendEmail::Helper.new.send_email_on_run_failure(
      Chef.run_context.node.name, exception.message
    )
  end
  on :run_completed do
    HandlerLog::Helper.new.save_log_on_event(
      Chef.run_context.node.name, "cleanup",Chef.run_context.node.run_list
    )
  end
end

#Remove default user Admin if exists
user "Admin" do
  action :remove
end

#Start service DHCP Client
windows_service "Dhcp" do
  action :configure_startup
  startup_type :automatic
  notifies :start, 'windows_service[Dhcp]', :delayed
end

windows_service "Dhcp" do
  action :nothing
end

#Get os
os = node['kernel']['os_info']['caption'].strip
#case
case os
when 'Microsoft Windows Server 2012 R2 Standard'
  #Configure local policy
  template 'C:\tmp\secpol2012.cfg' do
    source 'secpol2012.erb'
  end

  execute "import_user_rights" do
    command "secedit /configure /db c:\\windows\\security\\local.sdb /cfg c:\\tmp\\secpol2012.cfg /areas REGKEYS"
  end

  execute "import_security_policy" do
    command "secedit /configure /db c:\\windows\\security\\local.sdb /cfg c:\\tmp\\secpol2012.cfg /areas USER_RIGHTS"
  end
when 'Microsoft Windows Server 2008 R2 Standard'
  #Configure local policy
  template 'C:\tmp\secpol2008.cfg' do
    source 'secpol2008.erb'
  end

  execute "import_security_policy" do
    command "secedit /configure /db c:\\windows\\security\\local.sdb /cfg c:\\tmp\\secpol2008.cfg /areas REGKEYS"
  end
end

#GPO Update force to sync policy
execute "gpupdate_force" do
  command "gpupdate /force"
end

#Stop Open SSH Service
powershell_script "remove_tmp" do
 code <<-EOH
   Stop-Service sshd
   Set-Service sshd -startupType disabled
   EOH
   only_if "Test-Path C:\\tmp"
end

#Remove windows share
powershell_script "stop_ssh_service" do
 code <<-EOH
   Remove-PSDrive Z
   EOH
   only_if "Test-Path Z:"
end

#Remove user AdminASB if exists
user "AdminASB" do
  action :remove
end
