batch "force_installs" do
  code <<-EOH
    echo chef-client -o recipe[windows_os_asb::reactivate_windows] >> c:\\force.ps1
	echo del "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp\\Exec_force.bat" >> c:\\force.ps1
	echo del "c:\\force.ps1" >> c:\\force.ps1
 	echo exit >> c:\\force.ps1
	EOH
end

batch "exec_installs" do
  code <<-EOH
    cd "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\StartUp"
	echo powershell.exe -noexit -file "C:\\force.ps1" >> Exec_force.bat
	echo exit >> Exec_force.bat
	EOH
end

