#
# Cookbook Name:: windows_os_asb
# Recipe:: default
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#
line_num=0
text=File.open('c:\\windows\\Temp\\params.txt').read
text.gsub!(/\r\n?/, "\n")
text.each_line do |line|
  line_num += 1
  if line_num == 1
    node.default[:organizacion]=line.split(':')[1]
    #print "#{node.run_state[:organizacion]}"
  end
  if line_num == 2
    node.default[:ambiente]=line.split(':')[1]
    #print "#{node.run_state[:ambiente]}"
  end
  if line_num == 3
    node.default[:tipo]=line.split(':')[1]
    #print "#{node.run_state[:tipo]}"
  end
end