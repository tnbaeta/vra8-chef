require 'net/http'

module HandlerLog
  class Helper
    def send_email_on_run_failure(node_name, exception)
      emails = ['mguerram@bcp.com.pe','jorgeneirac@bcp.com.pe']
      subject = "Chef run failed on #{node_name} Date: #{Time.now.rfc2822}"
      message = "Error al ejecutar en la siguiente receta chef <br>"
      message << "#{exception}"
      emails.each do | email |
        uri = URI('http://picop01.credito.bcp.com.pe:20000/sendemail')
        params = { :to => "#{email}", :subject => subject, :message => message, :apikey => '0xfwb9030w=m08'  }
        uri.query = URI.encode_www_form(params)
        res = Net::HTTP.get_response(uri)
        puts res.body if res.is_a?(Net::HTTPSuccess)
      end
    end
	def save_log_on_event(node_name, recipe, run_list)
	print "#{node_name} #{recipe}"
    out_file = File.new("c:\\windows\\temp\\#{recipe}.txt", "w")
    out_file.puts("#{node_name} terminó #{run_list}")
    out_file.close
	end
  end
end
