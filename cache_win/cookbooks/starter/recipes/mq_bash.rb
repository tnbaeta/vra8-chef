# This is a Chef recipe file. It can be used to specify resources which will
# apply configuration to a server.

log "Welcome BCP to Chef !" do
  level :info
end

case node['platform']
when 'aix'

	#MQ Installation
	include_recipe "starter::mq_installation"
	
# If the platform is not AIX
else
	fail('Unsupported Operating System')
end

bash 'precheck' do
	code <<-EOH
	
	# NFS
	echo ""
	echo "Verificacion de repositorio NFS"
	echo ""
	mount pnetp03:/installPure /mnt
	if [ $? == 0 ]
	then
	echo "El punto de montaje nfs esta montado correctamente"
	echo ""
	else
	echo "Error al montar la unidad NFS"
	echo ""
	exit 1
	fi

	# datavg Validation
	lsvg applivg 1> /dev/null 2> /dev/null
	if [ $? == 0 ]
	then
	echo "El volume group applivg se encuentra creado correctamente"
	echo ""
	else
	echo "Error : No existe el volume group applivg"
	echo ""
	exit 1
	fi

	VGSIZE=`lsvg applivg | grep "FREE PPs" | awk '{print $7}' | sed 's/[^0-9]*//g'`
	if [ $VGSIZE -gt 20480 ]
	then
	echo "El volume group applivg tiene espacio suficiente"
	echo ""
	else
	echo "El volume group applivg no tiene espacio suficiente"
	exit 1
	fi
	EOH
	action :nothing
end

if 'precheck'
execute 'ok'
	command 'echo "Precheck - ok"'
else
execute 'wrong'
	command 'echo "Precheck - Fail"'
end


# For more information, see the documentation: https://docs.chef.io/essentials_cookbook_recipes.html
//