#receta para instalar antivirus
include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"
include_recipe "recursos_globales"


os = node['kernel']['os_info']['caption'].strip

#Copy files from repository
powershell_script "copy_installers" do
    code <<-EOH
    mkdir C:\\tmp\\Antivirus
    Copy-Item -Path Z:\\Antivirus\\* -Destination C:\\tmp\\Antivirus -Recurse
    EOH
    not_if "Test-Path C:\\tmp\\Antivirus\\McAfee\\uncompress\\setupEP.exe"
end


#Install Agente
case node['ambiente']
    when 'produccion'
        batch "av_agent_installation" do
            code <<-EOH
            "C:\\tmp\\Antivirus\\AgenteProd\\Agente_ EPO03\\McAfeeAgent 5.6.exe" /INSTALL=AGENT /SILENT
            EOH
        end   
    else
        # desarrollo y certificacion
        batch "av_agent_installation" do
            code <<-EOH
            "C:\\tmp\\Antivirus\\AgenteDesaCert\\Agente_EPO02\\FramePkg.exe" /INSTALL=AGENT /SILENT
            EOH
        end
end




#Install McAfee Antivirus
batch "av_installation" do
    code <<-EOH
    "C:\\tmp\\Antivirus\\McAfee\\uncompress\\setupEP.exe"  ADDLOCAL="tp"
    EOH
end



node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
     Punto: 9 - Antivirus y sus componentes\n
     Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)
