#GPO Update force to sync policy
execute "gpupdate_force" do
    command "gpupdate /force"
end

#Remove default user Admin if exists
user "Admin" do
    action :remove
end

#Stop Open SSH Service
powershell_script "remove_tmp" do
    code <<-EOH
      Stop-Service sshd
      Set-Service sshd -startupType disabled
      EOH
      only_if "Test-Path C:\\tmp"
end

#Remove windows share
powershell_script "stop_ssh_service" do
    code <<-EOH
      Remove-PSDrive Z
      EOH
      only_if "Test-Path Z:"
end

#Remove user AdminASB if exists
user "AdminASB" do
    action :remove
end

#Se quitan los instaladores dejados por chef

powershell_script "limpieza_temp_chef_folder" do
code <<-EOH
Remove-Item -Recurse -Force C:\\tmp
EOH
end