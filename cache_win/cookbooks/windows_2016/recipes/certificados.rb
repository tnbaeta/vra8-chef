include_recipe "recursos_globales"

#Disable insecure SSL Protocols and Ciphers [Deshabilita]
execute "SSL_Best_Practice" do
  command "Z:\\Util\\IISCryptoCli40.exe /best"
only_if "cd Z:"
end


#Desactivar PCT1.0
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\PCT 1.0\\Server" do
    values [
    {:name => 'Enabled', :type => :dword, :data => 0},
    {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
    recursive true
    action :create
end
  
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\PCT 1.0\\Client" do
    values [
    {:name => 'Enabled', :type => :dword, :data => 0},
    {:name => 'DisabledByDefault', :type => :dword, :data => 1}]
    recursive true
    action :create
end
  
#desactivar tls 1.0

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.0\\Server" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.0\\Client" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

#desactivar tls 1.1
registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.1\\Server" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.1\\Client" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end


#habilitar tls 1.2
registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.2\\Server" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 1},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\TLS 1.2\\Client" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 1},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

#desahabilitar ssl v2 y v3
registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 2.0\\Server" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 2.0\\Client" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 3.0\\Server" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Protocols\\SSL 3.0\\Client" do
  values [
  {:name => 'Enabled', :type => :dword, :data => 0},
  {:name => 'DisabledByDefault', :type => :dword, :data => 0}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\NULL" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\AES 128/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\AES 256/256" do
  values [{:name => 'Enabled', :type => :dword, :data => 1 }]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\DES 56/56" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end



registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC2 128/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC2 40/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC2 56/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC4 128/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC4 40/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC4 56/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\RC4 64/128" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\Triple DES 168/168" do
  values [{:name => 'Enabled', :type => :dword, :data => 1}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Ciphers\\Triple DES 168/168" do
  values [{:name => 'Enabled', :type => :dword, :data => 1}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Hashes\\MD5" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Hashes\\SHA" do
  values [{:name => 'Enabled', :type => :dword, :data => 0}]
  recursive true
  action :create
end

registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\Hashes\\SHA256" do
  values [{:name => 'Enabled', :type => :dword, :data => 1}]
  recursive true
  action :create
end


registry_key "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\SecurityProviders\\SCHANNEL\\KeyExchangeAlgorithms\\PKCS" do
  values [{:name => 'Enabled', :type => :dword, :data => 1}]
  recursive true
  action :create
end


#seteo cifrado correcto
powershell_script "certificates_cifrado" do
  guard_interpreter :powershell_script
  code <<-EOH
  Set-ItemProperty -path 'HKLM:\\SOFTWARE\\Policies\\Microsoft\\Cryptography\\Configuration\\SSL\\00010002' -name "Functions" -value "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA_P256,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA_P384,TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384_P384,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384_P384,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA_P256,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA_P384,TLS_DHE_DSS_WITH_AES_256_CBC_SHA256,TLS_DHE_DSS_WITH_AES_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384_P521,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384_P384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384_P256,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA_P521,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384_P521,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384_P521,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA_P521,TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_DHE_RSA_WITH_AES_256_CBC_SHA,TLS_PSK_WITH_AES_256_GCM_SHA384,TLS_PSK_WITH_AES_256_CBC_SHA384"
EOH
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Enero 2019\n
     Punto: 14 - Configuracion y aseguramiento  del protocolo ssl / tls \n
     Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea%20Base%20de%20Seguridad/BCP/Microsoft/Linea%20Base%20de%20Seguridad%20-%20Windows%202016%20Server.pdf
     Fuente Certificados: \\bcppoint\\DavWWWRoot\\gcmin\\dsyo\\seguridad\\sistemas\\Lnea Base de Seguridad\\BCP\\Certificados Digitales\\Linea Base de Seguridad para Certificados Digitales PKI v1.3.pdf
     "    
)
