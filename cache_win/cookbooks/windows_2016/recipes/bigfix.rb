#
# Cookbook Name:: windows_os_asb
# Recipe:: iem_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

#Check pre-requisites
execute 'start_windows_installer' do
command "sc start msiserver"
not_if 'sc query msiserver | find "RUNNING"'
end

#copy installers
case node['ambiente']
when 'produccion'
    powershell_script "copy_installers" do
    code <<-EOH
        Copy-Item -Path Z:\\Agentes\\IEM\\PROD\\* -Destination C:\\tmp -recurse
        EOH
        not_if "Test-Path C:\\tmp\\setup.exe"
    end
when 'certificacion'
    powershell_script "copy_installers" do
    code <<-EOH
        Copy-Item -Path Z:\\Agentes\\IEM\\CERT\\* -Destination C:\\tmp -recurse
        EOH
        not_if "Test-Path C:\\tmp\\setup.exe"
    end
when 'desarrollo'
    powershell_script "copy_installers" do
    code <<-EOH
        Copy-Item -Path Z:\\Agentes\\IEM\\DESA\\* -Destination C:\\tmp -recurse
        EOH
        not_if "Test-Path C:\\tmp\\setup.exe"
    end
end  

#run installer
batch "IEM_Install" do
code <<-EOH
Start /wait C:\\tmp\\setup.exe /S /v/qn
EOH
not_if "sc query besclient"
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Enero 2019\n
     Se instalo el agente BIGFIX"
)