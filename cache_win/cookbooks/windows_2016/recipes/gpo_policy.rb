#
# Recipe:: Aplica Reglas GPO servidores windows
# Author:: Jorge Copia
# Copyright 2019, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Windows 2016

include_recipe "recursos_globales"


#Interactive logon: Do not require CTRL+ALT+DEL.
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [ {:name => 'DisableCAD', :type => :dword, :data => 0}]
    recursive   true
    action      :create
end

#Interactive logon: Machine Inactivity limit
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [ {:name => 'InactivityTimeoutSecs', :type => :dword, :data => 900}]
    recursive   true
    action      :create
end

#Interactive logon: Require smart card
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [ {:name => 'ScForceOption', :type => :dword, :data => 0}]
    recursive   true
    action      :create
end


#Interactive logon: Smart card removal behavior
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon" do
    values [ {:name => 'ScRemoveOption', :type => :string, :data => 0}]
    recursive   true
    action      :create
end

#network access allow anonymous sid/name translation, no está claro cual es
# (HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Lsa\\TurnOffAnonymousBlock)

#network access: named pipes that can be accessed anonymously
#network access: shares that can be accessed anonymously
#Network access: Restrict anonymous access to Named Pipes and Shares
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\LanManServer\\Parameters" do
    values [ 
            {:name => 'NullSessionPipes', :type => :multi_string , :data => []},
            {:name => 'NullSessionShares', :type => :multi_string , :data => []},
            {:name => 'RestrictNullSessAccess', :type => :dword , :data => 1},            
     ]
    recursive   true
    action      :create
end

#NETWORK SECURITY: CONFIGURE ENCRYPTION TYPES ALLOWED FOR KERBEROS
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System\\Kerberos\\Parameters" do
    values [ {:name => 'SupportedEncryptionTypes', :type => :dword , :data => 2147483644 } ]
    recursive   true
    action      :create
end

#network security: lan manager authentication level
#system cryptography: use fips compliant algorithms for
#https://www.stigviewer.com/stig/windows_10/2017-02-21/finding/V-63811
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Lsa" do
    values [
        {:name => 'LmCompatibilityLevel', :type => :dword, :data => 5},
        {:name => 'FIPSAlgorithmPolicy', :type => :dword, :data => 0}
    ]
    recursive   true
    action      :create
end

#System Settings: Use Certificate Rules on Windows Executables for Software Restriction Policies
registry_key "HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\Safer\\CodeIdentifiers" do
    values [ {:name => 'AuthenticodeEnabled', :type => :dword, :data => 0}]
    recursive   true
    action      :create
end

#user account control: admin approval mode for the built-in administrator account
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [ {:name => 'FilterAdministratorToken', :type => :dword, :data => 1}]
    recursive   true
    action      :create
end


#User Account Control: Allow UIAccess applications to prompt for elevation without using the secure desktop
#User Account Control: Behavior of the elevation prompt for administrators in Admin Approval Mode
#User Account Control: Behavior of the elevation prompt for standard user
#User Account Control: Detect application installations and prompt for elevation
#User Account Control: only elevate uiaccess applications that are installed in secure locations
#User Account Control: switch to the secure desktop when promptin for elevation
#User Account Control: virtualize file and registry writes failures to per-user locations
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [
        {:name => 'EnableUIADesktopToggle', :type => :dword, :data => 0},
        {:name => 'ConsentPromptBehaviorAdmin', :type => :dword, :data => 5},
        {:name => 'ConsentPromptBehaviorUser', :type => :dword, :data => 3},
        {:name => 'EnableInstallerDetection', :type => :dword, :data => 1},
        {:name => 'ValidateAdminCodeSignatures', :type => :dword, :data => 0},
        {:name => 'EnableSecureUIAPaths', :type => :dword, :data => 1},
        {:name => 'EnableLUA', :type => :dword, :data => 1},
        {:name => 'PromptOnSecureDesktop', :type => :dword, :data => 1},
        {:name => 'EnableVirtualization', :type => :dword, :data => 1}
    ]
    recursive   true
    action      :create
end
