#
# Recipe:: lbs_general
# Author:: Jorge Copia Silva <jorcopia@pe.ibm.com>
# Copyright 2019, IBM
#
# All rights reserved - Do Not Redistribute

#Carga dependencias
include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"
include_recipe "recursos_globales"

#Library to generate randomPassword
#SecureRandom will be used for generate very secure password for best Security
require 'securerandom'


node["global_helper"].save_log( Chef.run_context.node.run_list, "Se comienza a aplicar linea base")


lbs_x_empresa = {
    "prima" => "windows_2016::subsidiarias_lbs", 
    "credicorp" => "windows_2016::subsidiarias_lbs", 
    "bcp" => "windows_2016::bcp_lbs"
}


randomPasswordFake = Array.new(12){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join + '$'

my_bag = data_bag_item("recursos","uncpath")
credentials = Chef::EncryptedDataBagItem.load("credentials", "windows","awkbash")


#Rename Guest user
powershell_script "rename_disable_guest_user" do
    guard_interpreter :powershell_script
    code <<-EOH
    $guest=[adsi]"WinNT://./Guest,user"
    $guest.psbase.rename("Invitado#{node['ambiente'][0..3].capitalize}")
    EOH
    only_if "if((net localgroup Guests | Where-Object {$_ -eq 'Guest'} | measure-object).count -eq 1){ $true } else { $false }"
end 


#observacion el cambio de password no es necesario porque por GPO (AD) esta bloqueado el login del usuario guest
#Cambia nombre del administrador
powershell_script "set_administrator_#{node['ambiente']}" do
  guard_interpreter :powershell_script
  code <<-EOH
  $u = [adsi]"WinNT://./Administrator,user"
  $u.psbase.rename("Admin#{node['ambiente'].capitalize}")
  $u.Description=""
  $u.SetInfo()
  $u.commitChanges()
  EOH
  only_if "if((net localgroup Administrators | Where-Object {$_ -eq 'Administrator'} | measure-object).count -eq 1){ $true } else { $false }"
end

#Habilita password never expire
powershell_script "set_administrator_flags" do
  guard_interpreter :powershell_script
  code <<-EOH
  $os = (Get-WmiObject -class Win32_OperatingSystem).Caption
  $u = [adsi]"WinNT://$env:computername/Admin#{node['ambiente'].capitalize},user"
  $flag = 65536
  $u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
  $u.commitChanges()
  EOH
end


#Create a fake Administrator user with complex password
user "Administrator" do
    action :create
    password "#{randomPasswordFake}"
end

#Set cant change password and decription to user Administrator
powershell_script "fake_administrator_password_policy" do
    guard_interpreter :powershell_script
    code <<-EOH
    $flag = 66112
    $u = [adsi]"WinNT://$env:computername/Administrator,user"
    $u.invokeSet("userFlags", ($u.userFlags[0] -BOR $flag))
    $u.Description="Built-in account for administering the computer/domain"
    $u.SetInfo()
    $u.commitChanges()
    EOH
    only_if "if((net localgroup Users | Where-Object {$_ -eq 'Administrator'} | measure-object).count -eq 1){ $true } else { $false }"
end


#Remove fake user Administrator from Users group
group "Users" do
    append true
    excluded_members ["Administrator"]
    action :modify
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "Cambio de credenciales del administrador y fake admin para BCP - CAPITAL - PRIMA"
)

#LLAMA AL LBS CORRESPONDIENTE A LA EMPRESA PARA LA QUE SE APROVISIONA
include_recipe "#{lbs_x_empresa[node["organizacion"]]}"