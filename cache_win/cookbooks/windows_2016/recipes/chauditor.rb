#
# Cookbook Name:: windows_os_asb
# Recipe:: chauditor_install
# Author:: Jorge Neira Chang, Pedro Acosta, Jorge Copia
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

include_recipe "recursos_globales"
include_recipe "bcp_common::tmp_directory"
include_recipe "bcp_common::windows_share"

node.default["global_helper"] = GlobalEvents::Helper.new
os_architecture=node['kernel']['os_info']['os_architecture'].strip  
  
#Check pre-requisites
directory "C:\\tmp\\ChangeAuditor" do
    action :create
    not_if "Test-Path C:\\tmp\\ChangeAuditor"
end

execute 'start_windows_installer' do
    command "sc start msiserver"
    not_if 'sc query msiserver | find "RUNNING"'
end


node["global_helper"].save_log( Chef.run_context.node.run_list, "Instalacion de Agente Change Chauditor")

#copy files
powershell_script "copy_installers" do
code <<-EOH
    Write-Host "Estoy copiando instaladores chauditor.........."
    mkdir C:\\tmp\\ChangeAuditor
    Copy-Item -Path Z:\\Agentes\\ChangeAuditor\\6.9.5025\\Installation -Destination C:\\tmp\\ChangeAuditor -Recurse
    EOH
    not_if "Test-Path C:\\tmp\\ChangeAuditor\\x64"
end


vmarch = {
    "64-bit" => "C:\\tmp\\ChangeAuditor\\Installation\\x64\\Dell Change Auditor Agent 6 (x64).msi",                                              
    "32-bit" => "C:\\tmp\\ChangeAuditor\\Installation\\x86\\Dell Change Auditor Agent 6 (x86).msi", 
}

print "\n\n\n\n <<<<<<<<<<<<<<<<<<< BEGIN CHAUDITOR_INSTALL >>>>>>>>>>>>>>>>>>>\n"

if node["ambiente"] == 'produccion' && node["organizacion"] == 'bcp'

    #Install prerequisito windows net framework 4
    powershell_script "install_netfx40" do
        cwd "C:\\tmp"
        code <<-EOH
        Set-ExecutionPolicy Bypass
        $installer = "C:\\tmp\\NDP452-KB2901907-x86-x64-AllOS-ENU.exe"
        $args = "/q /norestart"
        $process = (Start-Process -FilePath $installer -ArgumentList $args -Wait -Verb RunAs -PassThru)
        if($process.exitcode -eq 0){
        Write-Host "The netfx4 was installed successfully"
        }else{
        Write-Host "The netfx4 was installed"
        Write-Host $process.exitcode
        exit 0
        }
        EOH
        not_if "$R = @();$dotNetRegistry  = 'SOFTWARE\\Microsoft\\NET Framework Setup\\NDP';$dotNet4Registry = 'SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full';$dotNet4Builds = @{30319  =  '4.0';378389 = '4.5';378675 = '4.5.1 (8.1/2012R2)';378758 = '4.5.1 (8/7 SP1/Vista SP2)';379893 = '4.5.2';	380042 = '4.5 and later with KB3168275 rollup';393295 = '4.6 (Windows 10)';393297 = '4.6 (NON Windows 10)';394254 = '4.6.1 (Windows 10)';394271 = '4.6.1 (NON Windows 10)';394802 = '4.6.2 (Windows 10 Anniversary Update)';394806 = '4.6.2 (NON Windows 10)'};$Computer = '.';if($regKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $Computer)) {	if ($netRegKey = $regKey.OpenSubKey(\"$dotNetRegistry\")) {foreach ($versionKeyName in $netRegKey.GetSubKeyNames()) {if ($versionKeyName -match '^v[123]') {$versionKey = $netRegKey.OpenSubKey($versionKeyName);$version = [version]($versionKey.GetValue('Version', ''));$R+=New-Object -TypeName PSObject -Property @{ComputerName = $Computer;NetFXBuild = $version.Build;NetFXVersion = '' + $version.Major + '.' + $version.Minor} }}};if ($net4RegKey = $regKey.OpenSubKey(\"$dotNet4Registry\")) {if(-not ($net4Release = $net4RegKey.GetValue('Release'))) {$net4Release = 30319};$R+=New-Object -TypeName PSObject -Property @{ComputerName = $Computer;NetFXBuild = $net4Release;NetFXVersion = $dotNet4Builds[$net4Release]}}};if(@($R | Where-Object {$_.NetFXVersion -ge \"4.5.2\"} | Select-Object ComputerName, NetFXVersion, NetFXBuild ).Count -gt 0) { return $true} else { return $false }"
    end

    if os_architecture ==  '64-bit'    
        #Install Change Auditor 6.9 x64
        powershell_script "install_change_auditor_x64" do
            code <<-EOH
                Write-Host "<<<<<<<<<<<<<<<<< BEGIN install_change_auditor_x64 >>>>>>>>>>>>>>>>>>>>>>> \n"
                Set-ExecutionPolicy Bypass
                $installer = "msiexec.exe"
                $args = "/i `"#{vmarch[os_architecture]}`" /quiet /qb INSTALLATION_NAME_VALID=1 INSTALLATION_NAME=`"BCP`" REBOOT=`"ReallySuppress`" ALLUSERS=1 STAGINGPATH=`"C:\\Windows\\TEMP\\`""
                Write-Host "--------------------------------------------------"
                Write-Host $args
                Write-Host "--------------------------------------------------"
                $process = (Start-Process -FilePath $installer -ArgumentList $args -Wait -Verb RunAs -PassThru)
                if($process.exitcode -eq 0){
                Write-Host "The Change Auditor msi package installed successfully"
                }else{
                Write-Host "The Change Auditor msi package retuned a non expected exitcode"
                Write-Host $process.exitcode
                exit 0
                }
                Write-Host "<<<<<<<<<<<<<<<<< END install_change_auditor_x64 >>>>>>>>>>>>>>>>>>>>>>> \n"
            EOH
            not_if "if ((Get-Service -Name NPSrvHost -ErrorAction SilentlyContinue).Length -gt 0){ return $true } else { return $false }"
        end
    else
            #Install Change Auditor 6.9
        powershell_script "install_change_auditor_x86" do
        code <<-EOH
            Write-Host "<<<<<<<<<<<<<<<<< BEGIN install_change_auditor_x86 >>>>>>>>>>>>>>>>>>>>>>> \n"
            Set-ExecutionPolicy Bypass
            $installer = "msiexec.exe"
            $args = "/i `"#{vmarch[os_architecture]}`" /quiet /qb INSTALLATION_NAME_VALID=1 INSTALLATION_NAME=`"BCP`" REBOOT=`"ReallySuppress`" ALLUSERS=1 STAGINGPATH=`"C:\\Windows\\TEMP\\`""
            $process = (Start-Process -FilePath $installer -ArgumentList $args -Wait -Verb RunAs -PassThru)
            if($process.exitcode -eq 0){
            Write-Host "The Change Auditor msi package installed successfully"
            }else{
            Write-Host "The Change Auditor msi package retuned a non expected exitcode"
            Write-Host $process.exitcode
            exit 0
            }
        EOH
        not_if "if ((Get-Service -Name NPSrvHost -ErrorAction SilentlyContinue).Length -gt 0){ return $true } else { return $false }"
        end
    end

    node['global_helper'].save_log( 
        Chef.run_context.node.run_list, 
        "LINEA BASE: version: Agosto 2019\n
        Punto: 16 - Instalacion del Agente Change Chauditor\n
        Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
    )
else 
    print "<<<<<<<<<<< No instala chauditor, solo es valido en servidores de producion BCP>>>>>>\n"     
end