#
# Cookbook Name:: windows_agents_asb
# Recipe:: AgentSnow_install
# Author:: Joel Rodriguez Cerin, Pedro Acosta Gamarra
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

include_recipe "recursos_globales"

os=node['kernel']['os_info']['caption'].strip
os_architecture=node['kernel']['os_info']['os_architecture'].strip
node.default["global_helper"] = GlobalEvents::Helper.new

#Check pre-requisites
execute 'start_windows_installer' do
    command "sc start msiserver"
    not_if 'sc query msiserver | find "RUNNING"'
end

node["global_helper"].save_log( Chef.run_context.node.run_list, "Instalacion de SnowAgent")

#copy installers
powershell_script "copy_installers" do
    code <<-EOH
    Copy-Item -Path Z:\\Agentes\\Snow\\RedesLANTAN\\Windows\\* -Destination C:\\tmp -recurse
    EOH
    not_if "Test-Path C:\\tmp\\bancodecreditodelperu_snowagent531_x64.msi"
end

case os_architecture
    when '64-bit'
        #Install Snow Agent 64-bit
        execute "install_snow_agent" do
        command <<-EOF
        msiexec.exe /i C:\\tmp\\bancodecreditodelperu_snowagent531_x64.msi /l*v C:\\tmp\\SnowAgentinstall.log /qn
        EOF
        not_if "sc query Snow Inventory Client"
        end
    when '32-bit'
        #Install Snow Agent 32-bit
        execute "install_snow_agent" do
        command <<-EOF
        msiexec.exe /i C:\\tmp\\bancodecreditodelperu_snowagent531_x86.msi /l*v C:\\tmp\\SnowAgentinstall.log /qn
        EOF
        not_if "sc query Snow Inventory Client"
        end
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Enero 2019\n
    Punto: 19 - Instalacion del Agente Snow Inventory Client\n
    Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea%20Base%20de%20Seguridad/BCP/Microsoft/Linea%20Base%20de%20Seguridad%20-%20Windows%202016%20Server.pdf"
)