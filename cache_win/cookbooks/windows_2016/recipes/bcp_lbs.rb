#
# Recipe:: lbs_general
# Author:: Jorge Copia Silva <jorcopia@pe.ibm.com>
# Copyright 2019, IBM
#
# All rights reserved - Do Not Redistribute


#Configure EventLog
batch 'Set Log Size' do
    code <<-EOH
        wevtutil sl Application /ms:104857600
        wevtutil sl Security /ms:104857600
        wevtutil sl Setup /ms:33554432
        wevtutil sl System /ms:104857600
        wevtutil sl Microsoft-Windows-CertificateServicesClient-Lifecycle-System/Operational /ms:104857600
        wevtutil sl Microsoft-Windows-TerminalServices-LocalSessionManager/Operational /ms:104857600
        wevtutil sl Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational /ms:104857600
        EOH
end


node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
     Punto: 2 - Log Settings\n
     Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)

#Asignación al Grupo Administrators
group "Administrators" do
	action :modify
	members [
	 "BCPDOM\\Control de Cambios - Server Provisioning",
	 "BCPDOM\\SdS - Helpdesk Accesos y Permisos", 
	 "BCPDOM\\UGS Infraestr Generic",
	 "BCPDOM\\Innovation",
	 "BCPDOM\\Administracion de Almacenamiento",
	 "BCPDOM\\Monitoring COS Patch Management"
	]
	append true
end

#Asignación al Grupo Users
group "Users" do
    action :modify
    members [
        "BCPDOM\\Domain Users",	
        "NT AUTHORITY\\Authenticated Users",
        "NT AUTHORITY\\INTERACTIVE"
    ]
    append true
end


#Asignación al Grupo Power Users
group "Power Users" do
    action :modify
    members [
        "BCPDOM\\Control de Cambios - CMDB Admins",
        "BCPDOM\\Monitoring COS",
        "BCPDOM\\Control Operac - Sistemas Distribuidos",
        "BCPDOM\\MaD - Windows",
        "BCPDOM\\WME - Windows"
    ]
    append true
end
	
#Asignación al Grupo Backup Operators
group "Backup Operators" do
    action :modify
    members [
        "BCPDOM\\APTSMPRO",         
        "BCPDOM\\Control Operac - Sistemas Distribuidos", 
        "BCPDOM\\MaD - Windows",
        "BCPDOM\\WME - Windows"
    ]
    append true
end
	
#Asignación al Grupo Performance Monitor Users
group "Performance Monitor Users" do
    action :modify
    members [
        "BCPDOM\\Centro de Monitoreo Canales"    
    ]
    append true
end


#Asignacion al Grupo Remote Desktop Users
group "Remote Desktop Users" do
    action :modify
    members [
        "BCPDOM\\Centro de Monitoreo Canales",
        "BCPDOM\\Control de Cambios - CMDB Admins", 
        "BCPDOM\\Monitoring COS",
        "BCPDOM\\Control Operac - Sistemas Distribuidos",
        "BCPDOM\\MaD - Windows",
        "BCPDOM\\WME - Windows"
    ]
    append true
end


#GRUPOS PARA DESARROLLO Y CERTIFICACION
if node['ambiente'] == 'desarrollo' || node['ambiente'] == 'certificacion'

    group "Administrators" do
        action :modify
        members [ "BCPDOM\\Midrange - Windows CertDesa Admins"]		  
        append true
    end

    group "Remote Desktop Users" do
        action :modify
        members [ 
            "BCPDOM\\Control de Producción",
            "BCPDOM\\TaS - Sistemas Distribuidos",
        ]
        append true
    end

    group "Backup Operators" do
        action :modify
        members [
            "BCPDOM\\Control de Producción",
            "BCPDOM\\Centro de Monitoreo Canales",
        ]
        append true
    end

    group "Power Users" do
        action :modify
        members [
            "BCPDOM\\Control de Producción",
            "BCPDOM\\TaS - Sistemas Distribuidos",
            "BCPDOM\\Centro de Monitoreo Canales",
        ]
        append true
    end


    group "Performance Monitor Users" do
        action :modify
        members [
            "BCPDOM\\Control de Produccion"
        ]
        append true
    end

    #Asignación al Grupo Performance Log Users
    group "Performance Log Users" do
        action :modify
        members [
            "BCPDOM\\Centro de Monitoreo Canales", 
            "BCPDOM\\Control de Producción"]
        append true
    end

end 
	
case node['ambiente']
    when 'produccion'
        group "Administrators" do
        action :modify
        members [  
            "BCPDOM\\Control de Cambios - Release", 
            "BCPDOM\\Midrange - Windows Prod Admin",
            "BCPDOM\\Control de Cambios - Release_PAMM",
            "BCPDOM\\Control de Cambios - Server Provisioning_PAMM",
            "BCPDOM\\SDI - Windows_PAMM",
            "BCPDOM\\Sds - Helpdesk accesos y permisos_PAMM",
            "BCPDOM\\Innovation_PAMM",
            "BCPDOM\\Midrange - Windows Prod Admins_PAMM",
            "BCPDOM\\Administracion de Almacenamiento_PAMM",
            "BCPDOM\\Monitoring COS Patch Management_PAMM",
        ]
        append true
        end 

        #Asignación al Grupo Power Users
        group "Power Users" do
            action :modify
            members [
                "BCPDOM\\TaS - Sistemas Distribuidos_PAMM", 
                "BCPDOM\\Centro de Monitoreo Canales_PAMM",
                "BCPDOM\\Control de Produccion_PAMM",
                "BCPDOM\\Control Operac - Sistemas Distribuidos_PAMM",
                "BCPDOM\\MaD - Windows_PAMM",
                "BCPDOM\\WME - Windows_PAMM"
            ]
            append true
        end

        #Asignación al Grupo Backup Operators
        group "Backup Operators" do
            action :modify
            members [
                "BCPDOM\\Control de Produccion_PAMM", 
                "BCPDOM\\Centro de Monitoreo Canales_PAMM", 
                "BCPDOM\\Control Operac - Sistemas Distribuidos_PAMM", 
                "BCPDOM\\MaD - Windows_PAMM",
                "BCPDOM\\WME - Windows_PAMM"
            ]
            append true
        end

        group "Performance Monitor Users" do
            action :modify
            members [
                "BCPDOM\\Control de Produccion_PAMM",
                "BCPDOM\\Centro de Monitoreo Canales_PAMM"
            ]
            append true
        end

        #Asignación al Grupo Performance Log Users
        group "Performance Log Users" do
            action :modify
            members [
                "BCPDOM\\Centro de Monitoreo Canales_PAMM", 
                "BCPDOM\\Control de Produccion_PAMM"]
            append true
        end

        group "Remote Desktop Users" do
            action :modify
            members [
                "BCPDOM\\TaS - Sistemas Distribuidos_PAMM", 
                "BCPDOM\\Centro de Monitoreo Canales_PAMM",
                "BCPDOM\\Control de Produccion_PAMM",
                "BCPDOM\\Control Operac - Sistemas Distribuidos_PAMM",
                "BCPDOM\\MaD - Windows_PAMM",
                "BCPDOM\\WME - Windows_PAMM"
            ]
            append true
        end
        
    when 'certificacion'
        #Asignación al Grupo Administrators
        group "Administrators" do
        action :modify
        members [ 
            "BCPDOM\\Control de Cambios - Congelamientos en Certificacion"
        ]
        append true
        end
        
        #Asignación al Grupo Performance Monitor Users
        group "Performance Monitor Users" do
        action :modify
        members ["BCPDOM\\CdS - Equipo Estres"]
        append true
        end    
end

case node['tipo']
    when 'aplicacion'
        #Asignación al Grupo Administrators
        group "Administrators" do
        action :modify
        members ["BCPDOM\\SDI - Windows"]
        append true
        end
    else
        #Asignación al Grupo Administrators
        group "Administrators" do
        action :modify
        members ["BCPDOM\\Network Security - Windows"]
        append true
        end
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
     Punto: 3 -Usuarios y Grupos\n
     Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)


#Punto 8: Aseguramiento de llaves del Regestry
#Allow ICMP redirects
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\Tcpip\\Parameters" do
    values [{
    :name => 'EnableICMPRedirect',
    :type => :dword,
    :data => 0
    }]
    action :create
end

#Network Security: LAN Manager Authentication Level
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Lsa" do
    values [{
    :name => 'LmCompatibilityLevel',
    :type => :dword,
    :data => 5
    }]
    action :create
end

#Enable Automatic Logon
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon" do
    values [{
    :name => 'AutoAdminLogon',
    :type => :dword,
    :data => 0
    }]
    action :create
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
Punto: 8 - Aseguramiento de Llaves del Registry\n
Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)

#Disable IPv6
powershell_script "Disable_IPv6" do
    guard_interpreter :powershell_script
    code <<-EOH
    $NetAdapter = Get-NetAdapterBinding -DisplayName "Internet Protocol Version 6 (TCP/IPv6)"
    Disable-NetAdapterBinding -Name $NetAdapter.Name -ComponentID ms_tcpip6
    EOH
end

#Disable SNMPTRAP Service
service "SNMPTRAP" do
    action [:stop, :disable]
end
  
#Disable Spooler Service
service "Spooler" do
    action [:stop, :disable]
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
Punto: 10 - Deshabilitacion de Servicios y Features No Usados\n
Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)

#Remove Windows Features SMB1,TFTP-Client,SNMP-Service
powershell_script "Remove_Features" do
    guard_interpreter :powershell_script
    code <<-EOH
    Remove-WindowsFeature FS-SMB1
    Remove-WindowsFeature TFTP-Client
    Remove-WindowsFeature SNMP-Service
    EOH
end


#RDP Settings
powershell_script "Set_RDP_Service" do
    guard_interpreter :powershell_script
    code <<-EOH
    (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'").SetSecurityLayer(1)
    (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0)
    (Get-WmiObject -Class Win32_TSClientSetting -Namespace root\\cimv2\\TerminalServices -filter "TerminalName='RDP-tcp'" ).SetMaxMonitors(3)
    $settings=Get-WmiObject -class Win32_TSClientSetting -Namespace root\\cimv2\\terminalservices -Filter "TerminalName='RDP-tcp'"
    $settings.SetClientProperty("Drivemapping",1)
    $settings.SetClientProperty("AudioCaptureRedir",1)
    $settings.SetClientProperty("PNPRedirection",1)
    $settings.ConnectionPolicy=0
    $settings.DefaultToClientPrinter=0
    $settings.put()
    EOH
end

#Activate Default to Client Printer RDP Client Setting 
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Terminal Server\\WinStations\\RDP-Tcp" do
    values [{
    :name => 'fForceClientLptDef',
    :type => :dword,
    :data => 0
    }]
    action :create
end


node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Agosto 2019\n
Punto: 12 - Remote Desktop Service\n
Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea Base de Seguridad/BCP/Microsoft/Linea Base de Seguridad - Windows Server 2016 v1.1.pdf"
)


#No contemplado en LINEA BASE DE SEGURIDAD - PERO NECESARIO PARA EL DEPLOY

#Set Processor Scheduling
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\PriorityControl" do
    values [{
        :name => 'Win32PrioritySeparation',
        :type => :dword,
        :data => 18
    }]
    action :create
    end
    
#Disable Internal Firewall
powershell_script "Disable_Services" do
    guard_interpreter :powershell_script
    code <<-EOH
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
    EOH
end


#Remove Domain Admins 
group "Administrators" do
	action :modify
	excluded_members ["BCPDOM\\Domain Admins"]
	append true
end


#remove everyone from pagefile disk
execute "set permision" do
	command "icacls P:\\ /remove everyone"
	only_if "cd P:"
end
  
#Network Security: Configure encryption types allowed for Kerberos
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System\\Kerberos\\Parameters" do
	values [{
		:name => 'SupportedEncryptionTypes',
		:type => :dword,
		:data => 2147483644
	}]
	recursive true
	action :create
end

#UAC: Admin approval Mode for Built-in
registry_key "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" do
    values [{
    :name => 'FilterAdministratorToken',
    :type => :dword,
    :data => 1
    }]
    action :create
end

#Revision Vulnerabilidades: Globally Prevent Socket Hijacking
registry_key "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\Afd\\Parameters" do
    values [{
    :name => 'DisableAddressSharing',
    :type => :dword,
    :data => 1
    }]
    action :create
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "Finaliza LBS, con configuraciones adicionales"
)
