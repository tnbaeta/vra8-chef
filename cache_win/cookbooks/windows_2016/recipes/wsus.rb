#
# Cookbook Name:: windows_os_asb
# Recipe:: join_wsus
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
#

include_recipe "recursos_globales"


os = node['kernel']['os_info']['caption'].strip

#Delete wsus id
registry_key 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate' do
values [{:name => 'SusClientId', :type => :string, :data => ''},
        {:name => 'SusClientIdValidation', :type => :string, :data => ''}
        ]
action :delete
end

#Detener el servicio WSUS
execute "stop_wsus" do
command "net stop wuauserv"
only_if 'sc query wuauserv | find "RUNNING"'
end

#configurar el registro en WSUS
registry_key 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\windows\\WindowsUpdate' do
values [{:name => 'WUServer', :type => :string, :data => 'http://PWSUSP01.credito.bcp.com.pe:80'},
        {:name => 'WUStatusServer', :type => :string, :data => 'http://PWSUSP01.credito.bcp.com.pe:80'}
        ]
action :create
end

#Redistribution del servicio WSUS
batch "redistribution_wsus" do
code <<-EOH
    cd %systemroot%\\
    RD SoftwareDistribution /s /q
EOH
end

#continua la configuracion del registro en WSUS
registry_key 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\WindowsUpdate\\Auto Update' do
values [{:name => 'NextDetectionTime', :type => :string, :data => ''},
        {:name => 'BalloonTime', :type => :string, :data => ''},
        {:name => 'FeaturedUpdatesNotificationSeqNumGenTime', :type => :string, :data => ''},
        {:name => 'NextFeaturedUpdatesNotificationTime', :type => :string, :data => ''},
        {:name => 'NextSqmReportTime', :type => :string, :data => ''},
        {:name => 'ScheduledInstallDate', :type => :string, :data => ''}
        ]
action :delete
end

#Reiniciando servicios WSUS
batch "reiniciando_wsus" do
code <<-EOH
    net stop cryptsvc
    net start cryptsvc
    net start wuauserv
    net stop bits
    net start bits
    wuauclt /resetautorizathion
    wuauclt /resetautorizathion
    wuauclt /resetautorizathion
    wuauclt /resetautorizathion
    wuauclt /resetautorizathion
    wuauclt /resetautorizathion
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /reportnow
    wuauclt /detectnow
    wuauclt /detectnow
    wuauclt /detectnow
    wuauclt /detectnow
    wuauclt /detectnow
    wuauclt /downloadnow
    wuauclt /downloadnow
    wuauclt /downloadnow
EOH
end

#Report and download windows patches
batch "download_patches" do
code <<-EOH
wuauclt /resetautorizathion
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow 
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /reportnow
wuauclt /detectnow
wuauclt /downloadnow
    EOH
end

case os
when 'Microsoft Windows Server 2016 Standard'
powershell_script "download-and install-automatic-updates" do
    guard_interpreter :powershell_script
    code <<-EOH
    c:\\windows\\system32\\UsoClient.exe startscan
    c:\\windows\\system32\\UsoClient.exe StartDownload
    c:\\windows\\system32\\UsoClient.exe StartInstall
    (New-Object -ComObject Microsoft.Update.AutoUpdate).DetectNow()
    EOH
    end
else
    #Install wsus patches
    # Configures Windows Update automatic updates
    powershell_script "configure-automatic-updates" do
    guard_interpreter :powershell_script
    code <<-EOH
        Write-Host -ForegroundColor Green "Searching for updates (this may take up to 30 minutes or more)..."
        $updateSession = New-Object -com Microsoft.Update.Session
        $updateSearcher = $updateSession.CreateupdateSearcher()
        $searchResult =  $updateSearcher.Search("Type='Software' and IsHidden=0 and IsInstalled=0").Updates
        foreach ($updateItem in $searchResult) {
        $UpdatesToDownload = New-Object -com Microsoft.Update.UpdateColl
        if (!($updateItem.EulaAccepted)) {
            $updateItem.AcceptEula()
        }
        $UpdatesToDownload.Add($updateItem)
        $Downloader = $UpdateSession.CreateUpdateDownloader()
        $Downloader.Updates = $UpdatesToDownload
        $Downloader.Download()
        $UpdatesToInstall = New-Object -com Microsoft.Update.UpdateColl
        $UpdatesToInstall.Add($updateItem)
        $Title = $updateItem.Title
        Write-host -ForegroundColor Green "  Installing Update: $Title"
        $Installer = $UpdateSession.CreateUpdateInstaller()
        $Installer.Updates = $UpdatesToInstall
        $InstallationResult = $Installer.Install()
        eventcreate /t INFORMATION /ID 1 /L APPLICATION /SO "Chef-Cookbook" /D "InstallWindowsUpdates: Installed update $Title."
        }
        eventcreate /t INFORMATION /ID 999 /L APPLICATION /SO "Chef-Cookbook" /D "InstallWindowsUpdates: Done Installing Updates."
    EOH
    action :run
    end
end

node['global_helper'].save_log( 
    Chef.run_context.node.run_list, 
    "LINEA BASE: version: Enero 2019\n
     Punto: 11 - Parches de Seguridad\n
     Fuente: url: http://bcppoint/gcmin/dsyo/seguridad/sistemas/Lnea%20Base%20de%20Seguridad/BCP/Microsoft/Linea%20Base%20de%20Seguridad%20-%20Windows%202016%20Server.pdf"
)