[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#### Chef client installation START #####

## Set web server variables
$webServer = "http://ad-01.illfact.lab/downloads/apps"


## Set chef variables
$hosts = "192.168.110.52 chef-01.illfact.lab"
$file = "C:\Windows\System32\drivers\etc\hosts"
$hosts | Add-Content $file
$chefServer = "chef-01.illfact.lab"
$chefOrg = "illfact"
$chefServerUrl = "https://$chefServer/organizations/$chefOrg"

## Download the Chef Client
$clientURL = "$webServer/chef-client-16.7.61-1-x64.msi"
$clientDestination = "C:\chef-client.msi"
Invoke-WebRequest $clientURL -OutFile $clientDestination

## Install the Chef Client
Start-Process msiexec.exe -ArgumentList @('/qn', '/lv C:\Windows\Temp\chef-log.txt', '/i C:\chef-client.msi', 'ADDLOCAL="ChefClientFeature,ChefSchTaskFeature,ChefPSModuleFeature"') -Wait
Remove-Item $clientDestination

## Download Chef validator key
$validatorURL = "$webServer/$chefOrg-validator.pem"
$validatorDestination = "C:\chef\validator.pem"

Invoke-WebRequest $validatorURL -OutFile $validatorDestination

## Create first-boot.json
$firstboot = @{
   "run_list" = @("recipe[win_base]")
}
Set-Content -Path c:\chef\first-boot.json -Value ($firstboot | ConvertTo-Json -Depth 10)

## Create client.rb

$clientrb = @"
chef_license           'accept'
ssl_verify_mode        ':verify_none'
chef_server_url        '$chefServerUrl'
local_key_generation   'true'
validation_key         '$validatorDestination'
log_location           'C:\chef\chef.log'
"@

Set-Content -Path c:\chef\client.rb -Value $clientrb

## Run Chef
C:\opscode\chef\bin\chef-client.bat -j C:\chef\first-boot.json 