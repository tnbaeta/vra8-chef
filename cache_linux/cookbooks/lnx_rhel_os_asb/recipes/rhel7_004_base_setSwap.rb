#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_004_base_setSwap
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_004_base_setSwap" do
  level :info
end

bash "rhel7_004_base_setSwap_01" do
  cwd "/tmp"
  code <<-EOH
   mkdir /fixes/chef > /dev/null 2>&1
   echo "sed -e 's/\\s*\\([\\+0-9a-zA-Z]*\\).*/\\1/' << EOF | fdisk /dev/sdb" > /fixes/chef/fdisk.sh
   echo -e "\\tn # new partition" >> /fixes/chef/fdisk.sh
   echo -e "\\tp # primary partition" >> /fixes/chef/fdisk.sh
   echo -e "\\t1 # partition number 1" >> /fixes/chef/fdisk.sh
   echo -e "\\t  # First cylinder (1-12288, default 1): " >> /fixes/chef/fdisk.sh
   echo -e "\\t  # Last cylinder, +cylinders or +size{K,M,G} (1-12288, default 12288):" >> /fixes/chef/fdisk.sh
   echo -e "\\tt # Change partition's system id" >> /fixes/chef/fdisk.sh
   echo -e "\\tL # list Codes" >> /fixes/chef/fdisk.sh
   echo -e "\\t8e # Linux LVM" >> /fixes/chef/fdisk.sh
   echo -e "\\tw # write the partition table" >> /fixes/chef/fdisk.sh
   echo EOF >> /fixes/chef/fdisk.sh
   chmod 750 /fixes/chef/fdisk.sh
   /fixes/chef/fdisk.sh 2>&1
   sleep 5
   rm -f /fixes/chef/fdisk.sh > /dev/null 2>&1
   rmdir /fixes/chef > /dev/null 2>&1
  EOH
  not_if { ::File.exist?('/tmp/rhel7_004_base_setSwap_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se asigna Disco Linux LVM.\" >> /tmp/rhel7_004_base_setSwap_01.log"
end

bash "rhel7_004_base_setSwap_02" do
  cwd "/tmp"
  code <<-EOH
   #Configurar SWAP
   vgcreate vg_swap /dev/sdb1
   lvcreate vg_swap -n lv_swap01 -l 100%FREE
   mkswap -L swap -f /dev/vg_swap/lv_swap01
   echo "/dev/vg_swap/lv_swap01     swap    swap 0 0" >> /etc/fstab
   swapoff -a > /dev/null 2>&1
   swapon -a > /dev/null 2>&1
   sleep 5
   swapon -a > /dev/null 2>&1
  EOH
  not_if { ::File.exist?('/dev/sdb1') }
end

execute 'Local Log02' do
  command "echo \" `date` : Se configura SWAP.\" >> /tmp/rhel7_004_base_setSwap_02.log"
end

