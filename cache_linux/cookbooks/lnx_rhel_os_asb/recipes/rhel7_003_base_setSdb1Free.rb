#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_003_base_setSdb1Free
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_003_base_setSdb1Free" do
  level :info
end

bash "rhel7_003_base_setSdb1Free_01" do
  cwd "/tmp"
  code <<-EOH
   mkdir /fixes/chef > /dev/null 2>&1
   echo "sed -e 's/\\s*\\([\\+0-9a-zA-Z]*\\).*/\\1/' << EOF | fdisk /dev/sdb" > /fixes/chef/fdisksdb1.sh
   echo -e "\\tp # new partition" >> /fixes/chef/fdisksdb1.sh
   echo -e "\\td # primary partition" >> /fixes/chef/fdisksdb1.sh
   echo -e "\\tw # partition number 1" >> /fixes/chef/fdisksdb1.sh
   echo EOF >> /fixes/chef/fdisksdb1.sh
   chmod 750 /fixes/chef/fdisksdb1.sh
   /fixes/chef/fdisksdb1.sh 2>&1
   sleep 5
   rm -f /fixes/chef/fdisksdb1.sh > /dev/null 2>&1
   rmdir /fixes/chef > /dev/null 2>&1
   shred -v /dev/sdb     2>&1
  EOH
  not_if { ::File.exist?('/tmp/rhel7_003_base_setSdb1Free_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se Elimina sdb1.\" >> /tmp/rhel7_003_base_setSdb1Free_01.log"
end

