#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_005_base_setUsers
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_005_base_setUsers" do
  level :info
end

bash "reseteo_masivo" do
  cwd "/tmp"
  code <<-EOH
   #Lista usuarios creados
   cat /etc/passwd > /tmp/users_list.txt
   #Asigna nuevo password a usuarios existentes
   i=0;while read line;do i=$(($i+1)) matricula=$(echo $line | cut -d ':' -f1) perfil=$(echo $line | cut -d ':' -f3) correo="s"$(echo $matricula | cut -d 's' -f2)"@credito.bcp.com.pe" password=$(openssl rand -hex 4);if [ $i -ne 1 ] && [ "$perfil" -gt 999 ];then echo "$matricula,$correo,$password" >> /tmp/passwords.txt;fi; done < "/tmp/users_list.txt"
   #Cambio de passwords
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1) password=$(echo $line | cut -d ',' -f3);echo "$matricula:$password" | chpasswd;done < "/tmp/passwords.txt" >>/tmp/cambio.txt
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1);chage -d 0 $matricula;done < "/tmp/passwords.txt" >>/tmp/cambio.txt
  EOH
  not_if { ::File.exist?('/tmp/users_list.txt') }
end

bash "envio_masivo" do
  cwd "/tmp"
  code <<-EOH
   #Guarda Host
   host=$(hostname)
   #Guarda IP
   IP=$(ifconfig | grep -A 1 'ens192' | tail -1 | cut -d 't' -f2 | cut -d 'n' -f1 | cut -d ' ' -f2 | cut -d ' ' -f1)
   #Envia correo
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1) correo=$(echo $line | cut -d ',' -f2) password=$(echo $line | cut -d ',' -f3);if [ -n "$correo" ];then curl http://picop01.credito.bcp.com.pe:20000/sendemail?to=$correo\\&subject=ICO%20PRODUCCION:%20Envio%20de%20credenciales\\&message=Se%20le%20informa%20que%20sus%20credenciales%20en%20el%20servidor%20$host%20con%20IP%20$IP%20es%20la%20siguiente%3A%20%3Cbr%3E%3Cbr%3E%20usuario%3A$matricula%20%3Cbr%3E%20password%3A$password\\&apikey=0xfwb9030w=m08;sleep 3;fi;done < "/tmp/passwords.txt" >> /tmp/sendemail.txt
  EOH
  only_if { ::File.exist?('/tmp/users_list.txt') }
end

bash "elimina_temporales" do
  cwd "/tmp"
  code <<-EOH
   #Lista usuarios creados
   rm -f /tmp/cambio.txt
   rm -f /tmp/users_list.txt
   rm -f /tmp/passwords.txt
  EOH
  only_if { ::File.exist?('/tmp/users_list.txt') }
end



