#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_001_base_setHostname
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_001_base_setHostname" do
  level :info
end

execute 'Set Hostname' do
  command "export host=$(hostname | cut -d '.' -f1) ; hostnamectl set-hostname $host"
end

execute 'Local Log' do
  command "echo \" `date` : Cambio de Hostname Realizado.\" >> /tmp/rhel7_001_base_setHostname.log"
end