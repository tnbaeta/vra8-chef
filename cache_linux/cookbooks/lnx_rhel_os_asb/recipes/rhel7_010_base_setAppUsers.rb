#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_010_base_setAppUsers
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_010_base_setAppUsers" do
  level :info
end

dynamic = Chef::EncryptedDataBagItem.load("appUsers", "dynamic","awkbash")
dynamic_user = dynamic["usuario"]
dynamic_pass = dynamic["password"]
ioala = Chef::EncryptedDataBagItem.load("appUsers", "ioala","awkbash")
ioala_user = ioala["usuario"]
ioala_pass = ioala["password"]
usrpam = Chef::EncryptedDataBagItem.load("appUsers", "usrfapro","awkbash")
usrpam_user = usrpam["usuario"]
usrpam_pass = usrpam["password"]
usrpamScan = Chef::EncryptedDataBagItem.load("appUsers", "usrscpro","awkbash")
usrpamScan_user = usrpam["usuario"]
usrpamScan_pass = usrpam["password"]

#Creación de usuarios de aplicación
bash "rhel7_010_base_setAppUsers_01" do
  cwd "/tmp"
  code <<-EOH
  if [ -d /etc/sudoers.d ]
   then
    chmod 755 /etc/sudoers.d
   else 
    mkdir /etc/sudoers.d
    chmod 755 /etc/sudoers.d
   fi
   #Usuario Dynamic Automation
   ##Grupo
   groupadd automata
   ##Creación de Usuarios
   /usr/sbin/useradd -c 'Usuario Dynamic Automation' -g automata #{dynamic_user}
   /usr/sbin/useradd -c 'Usuario de IOALA' -G bin,sys #{ioala_user}
   /usr/sbin/useradd -c 'usrfapro' -g GAIOHDAP -G GAIOHDAP #{usrpam_user}
   /usr/sbin/useradd -c 'usrscpro' -g GAIOMIDR -G GAIOMIDR #{usrpamScan_user}
   /usr/sbin/useradd -u 600 -g GSINESEC -G bin,sys,adm,GSINESEC usrnsec
   ##Asignación de Password
   echo "#{dynamic_user}:#{dynamic_pass}" | chpasswd
   echo "#{ioala_user}:#{ioala_pass}" | chpasswd
   echo "#{usrpam_user}:#{usrpam_pass}" | chpasswd
   echo "#{usrpamScan_user}:#{usrpamScan_pass}" | chpasswd
   ##No expira password
   chage -I -1 -m 0 -M -1 -E -1 #{dynamic_user}
   chage -I -1 -m 0 -M -1 -E -1 #{ioala_user}
   chage -E -1 -M -1 -I -1 #{usrpam_user}
   chage -E -1 -M -1 -I -1 #{usrpamScan_user}
   chage -E -1 -M -1 -I -1 usrnsec
   
   ##No Reset
   pam_tally2 --user #{dynamic_user} --reset
   pam_tally2 --user #{ioala_user} --reset
   pam_tally2 --user #{usrpam_user} --reset
   pam_tally2 --user #{usrpamScan_user} --reset
  EOH
  not_if { ::File.exist?('/tmp/rhel7_010_base_setAppUsers_01.log') }
end

#Creación de archivo Sudoer
cookbook_file '/etc/sudoers.d/123_AE_GLB_V2.0.1_SIN_NEGADOS' do
 source '123_AE_GLB_V2.0.1_SIN_NEGADOS'
 action :create
 mode '0400'
 not_if { ::File.exist?('/etc/sudoers.d/123_AE_GLB_V2.0.1_SIN_NEGADOS') }
end

#Asignación de permisos
bash "rhel7_010_base_setAppUsers_02" do
  cwd "/tmp"
  code <<-EOH
   chown root:root /etc/sudoers.d/123_AE_GLB_V2.0.1_SIN_NEGADOS
  EOH
  only_if { ::File.exist?('/etc/sudoers.d/123_AE_GLB_V2.0.1_SIN_NEGADOS') }
end