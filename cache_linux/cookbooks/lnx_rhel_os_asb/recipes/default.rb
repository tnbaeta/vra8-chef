#
# Cookbook Name:: lnx_rhel_os_asb
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe "lnx_rhel_os_asb::rhel7_001_base_setHostname"
include_recipe "lnx_rhel_os_asb::rhel7_002_base_setIP"
include_recipe "lnx_rhel_os_asb::rhel7_003_base_setSdb1Free"
include_recipe "lnx_rhel_os_asb::rhel7_004_base_setSwap"
include_recipe "lnx_rhel_os_asb::rhel7_005_base_setUsers"
include_recipe "lnx_rhel_os_asb::rhel7_006_base_setSubscription"
include_recipe "lnx_rhel_os_asb::rhel7_007_base_setCredentials"
include_recipe "lnx_rhel_os_asb::rhel7_008_base_setVMwareTools"
include_recipe "lnx_rhel_os_asb::rhel7_009_base_setRootPassword"