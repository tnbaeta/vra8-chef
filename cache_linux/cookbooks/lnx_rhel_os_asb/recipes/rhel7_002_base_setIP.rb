#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_002_base_setIP
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_002_base_setIP" do
  level :info
end

bash "rhel7_002_base_setIP_01" do
  cwd "/tmp"
  code <<-EOH
   #Configurar Dispositivo de Red
   echo IPV4_FAILURE_FATAL=no >> /etc/sysconfig/network-scripts/ifcfg-ens192
   echo IPV6INIT=yes >> /etc/sysconfig/network-scripts/ifcfg-ens192
   echo IPV6_AUTOCONF=yes >> /etc/sysconfig/network-scripts/ifcfg-ens192
   echo IPV6_DEFROUTE=yes >> /etc/sysconfig/network-scripts/ifcfg-ens192
   echo IPV6_FAILURE_FATAL=no >> /etc/sysconfig/network-scripts/ifcfg-ens192
   #rm -f /etc/sysconfig/network-scripts/ifcfg-eth0
   service network restart
  EOH
  not_if { ::File.exist?('/tmp/rhel7_002_base_setIP_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se configura Tarjeta de Red.\" >> /tmp/rhel7_002_base_setIP_01.log"
end

bash "rhel7_002_base_setIP_02" do
  cwd "/tmp"
  code <<-EOH
   #Configurar DNS
   echo nameserver 130.1.40.10 > /etc/resolv.conf
   echo nameserver 131.1.20.41 >> /etc/resolv.conf
   echo domain credito.bcp.com.pe >> /etc/resolv.conf
   service network restart
  EOH
  not_if { ::File.exist?('/tmp/rhel7_002_base_setIP_02.log') }
end

execute 'Local Log02' do
  command "echo \" `date` : Se configura DNS.\" >> /tmp/rhel7_002_base_setIP_02.log"
end