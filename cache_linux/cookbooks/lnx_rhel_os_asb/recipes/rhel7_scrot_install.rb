#
# Cookbook Name:: lnx_rhel_os_asb
# Recipe:: rhel7_scrot_install
# Author:: Joel Rodriguez Cerin
# Copyright 2019, IBM
#
# Fecha Creacion:: 23.01.2019
# All rights reserved - Do Not Redistribute
# Environment Production

#Elimina archivos temporales
file '/opt/scrot_08_19fc29_x86_64.rpm' do
  action :delete
end

directory '/tmp/nimldr_linux/' do
  recursive true
  action :delete
end

#Descarga RPM de Scrot
cookbook_file '/opt/scrot_08_19fc29_x86_64.rpm' do
 source 'scrot_08_19fc29_x86_64.rpm'
 action :create
 mode '0755'
 not_if { ::File.exist?('/opt/scrot_08_19fc29_x86_64.rpm') }
end

bash "instalacion de scrot" do
  cwd "/tmp"
  code <<-EOH
	echo "Instaladores copiados" >> "/tmp/trace-Scrot.log"
	rpm -ivh /opt/scrot_08_19fc29_x86_64.rpm >> "/tmp/instala_Scrot.log"
	echo "Se ejecuta instalador" >> "/tmp/trace-Scrot.log"
	rm -f /opt/scrot_08_19fc29_x86_64.rpm
	echo "Se eliminan instaladores" >> "/tmp/trace-Scrot.log"
  EOH
  only_if { ::File.exist?('/opt/scrot_08_19fc29_x86_64.rpm') }
end

