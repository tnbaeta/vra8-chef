#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_005_base_setUsers
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_005_base_setUsers" do
  level :info
end

bash "rhel7_005_base_setUsers_01" do
  cwd "/tmp"
  code <<-EOH
   #Copiamos archivos via BigFix
   IP=$(ifconfig | grep -A 1 'ens192' | tail -1 | cut -d 't' -f2 | cut -d 'n' -f1 | cut -d ' ' -f2 | cut -d ' ' -f1)
   echo '<?xml version="1.0" encoding="utf-8"?>' >/tmp/action.xml
   echo '<BES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BES.xsd">' >>/tmp/action.xml
   echo ' <SourcedFixletAction>' >>/tmp/action.xml
   echo '   <SourceFixlet>' >>/tmp/action.xml
   echo '     <Sitename>BCP Innovation Automata</Sitename>' >>/tmp/action.xml
   echo '     <FixletID>62412</FixletID>' >>/tmp/action.xml
   echo '     <Action>Action1</Action>' >>/tmp/action.xml
   echo '   </SourceFixlet>' >>/tmp/action.xml
   echo '   <Target>' >>/tmp/action.xml
   echo '     <ComputerName>PICOCHEFP01</ComputerName>' >>/tmp/action.xml
   echo '   </Target>' >>/tmp/action.xml
   echo '   <Parameter Name="IP">'$IP'</Parameter>' >>/tmp/action.xml
   echo ' </SourcedFixletAction>' >>/tmp/action.xml
   echo '</BES>' >>/tmp/action.xml
   curl -X POST --data-binary @/tmp/action.xml --user iemapp:h4bahihx -k https://10.80.129.152:52311/api/actions
   while [ ! -f /tmp/issue.net ]; do   sleep 2; done
   while [ ! -f /tmp/sudoers ]; do   sleep 2; done
   while [ ! -f /tmp/usuarios_bcp.csv ]; do   sleep 2; done
   while [ ! -f /tmp/usuarios_bcp_cert_desa.csv ]; do   sleep 2; done
   while [ ! -f /tmp/usuarios_ibm.csv ]; do   sleep 2; done    
   while [ ! -f /tmp/usuarios_ibm_cert_desa.csv ]; do   sleep 2; done   
   while [ ! -f /tmp/adduser_ibm.sh ]; do   sleep 2; done
   while [ ! -f /tmp/adduser_bcp.sh ]; do   sleep 2; done
  EOH
  not_if { ::File.exist?('/tmp/rhel7_005_base_setUsers_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se copia archivos de Usuarios\" >> /tmp/rhel7_005_base_setUsers_01.log"
end


bash "rhel7_005_base_setUsers_02" do
  cwd "/tmp"
  code <<-EOH
   #Repositorio Ejecutables
   mkdir /fixes/lbs > /dev/null 2>&1 
   #GRUPOS BCP
   cat /dev/null > /tmp/grupos.txt
   cat /dev/null > /tmp/passwords.txt
   cat /dev/null > /tmp/cambio.txt
   i=0; while read line; do i=$(($i+1)) grupo=$(echo $line | cut -d ',' -f4); if [ $i -ne 1 ] && [ -n "$grupo" ];then groupadd -f "$grupo"; echo "$grupo"; fi; done < "/tmp/usuarios_bcp.csv" >>/tmp/grupos.txt
   #USUARIOS BCP
   i=0;while read line;do i=$(($i+1)) nombre=$(echo $line | cut -d ',' -f2) matricula=$(echo $line | cut -d ',' -f3) grupo=$(echo $line | cut -d ',' -f4) valida=$(echo $matricula | cut -d 's' -f1) password=$(openssl rand -hex 4);if [ "$valida" == "a" ];then correo="s"$(echo $matricula | cut -d 's' -f2)"@credito.bcp.com.pe"; else correo=$(echo $matricula)"@credito.bcp.com.pe";fi;if [ $i -ne 1 ] && [ -n "$nombre" ];then echo 'useradd -c "'$nombre'" -d /home/'$matricula' -g '$grupo' -s /bin/bash -p '$password $matricula >>/tmp/usuarios-bcp.sh; echo "$matricula,$correo,$password" >> /tmp/passwords.txt;fi; done < "/tmp/usuarios_bcp.csv"
   mv /tmp/usuarios-bcp.sh /fixes/lbs/usuarios-bcp.sh
   chmod 550 /fixes/lbs/usuarios-bcp.sh
   /fixes/lbs/usuarios-bcp.sh
   #GRUPOS IBM
   i=0; while read line; do i=$(($i+1)) grupo=$(echo $line | cut -d ',' -f4); if [ $i -ne 1 ] && [ -n "$grupo" ];then groupadd -f "$grupo"; echo "$grupo"; fi; done < "/tmp/usuarios_ibm.csv" >>/tmp/grupos.txt
   #USUARIOS IBM
   i=0;while read line;do i=$(($i+1)) nombre=$(echo $line | cut -d ',' -f2) matricula=$(echo $line | cut -d ',' -f3) grupo=$(echo $line | cut -d ',' -f4) valida=$(echo $matricula | cut -d 's' -f1) password=$(openssl rand -hex 4);if [ "$valida" == "a" ];then correo="s"$(echo $matricula | cut -d 's' -f2)"@credito.bcp.com.pe"; else correo=$(echo $matricula)"@credito.bcp.com.pe";fi;if [ $i -ne 1 ] && [ -n "$nombre" ];then echo 'useradd -c "'$nombre'" -d /home/'$matricula' -g '$grupo' -s /bin/bash -p '$password $matricula >>/tmp/usuarios-ibm.sh; echo $matricula,$correo,$password >> /tmp/passwords.txt;fi; done < "/tmp/usuarios_ibm.csv"
   mv /tmp/usuarios-ibm.sh /fixes/lbs/usuarios-ibm.sh
   chmod 555 /fixes/lbs/usuarios-ibm.sh
   /fixes/lbs/usuarios-ibm.sh
   #CAMBIO DE PASSWORD
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1) password=$(echo $line | cut -d ',' -f3);echo "$matricula:$password" | chpasswd;done < "/tmp/passwords.txt" >>/tmp/cambio.txt
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1);chage -d 0 $matricula;done < "/tmp/passwords.txt" >>/tmp/cambio.txt
   #SUDOERS & ISSUE.NET
   cp /tmp/sudoers /etc/sudoers
   cp /tmp/issue.net /etc/issue.net
  EOH
  not_if { ::File.exist?('/tmp/rhel7_005_base_setUsers_02.log') }
end


execute 'Local Log02' do
  command "echo \" `date` : Se copia archivos de Usuarios\" >> /tmp/rhel7_005_base_setUsers_02.log"
end
