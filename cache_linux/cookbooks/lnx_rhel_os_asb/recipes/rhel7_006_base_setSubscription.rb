#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_006_base_setSubscription
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_006_base_setSubscription" do
  level :info
end


bash "rhel7_006_base_setSubscription_01" do
  cwd "/tmp"
  code <<-EOH
   #Configuracion de Proxy para la suscripcion 
   subscription-manager config --server.proxy_hostname=balanceadorws.credito.bcp.com.pe --server.proxy_port=8080
   subscription-manager register --username javiergutierrezv@bcp.com.pe --password 'P2c1f1c0$$.1' --auto-attach --force 
  EOH
  not_if { ::File.exist?('/tmp/rhel7_006_base_setSubscription_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se configura la Suscripcion\" >> /tmp/rhel7_006_base_setSubscription_01.log"
end