	#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_007_base_setCredentials
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_007_base_setCredentials" do
  level :info
end

bash "rhel7_007_base_setCredentials_01" do
  cwd "/tmp"
  code <<-EOH
   #netsec_mail=NetworkSecurity@credito.bcp.com.pe
   #netsec_mail=mguerram@bcp.com.pe
   #Repositorio Ejecutables
   #function sendmail { echo '<?xml version="1.0" encoding="utf-8"?>' >/tmp/sendmail.xml; echo '<BES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="BES.xsd">' >>/tmp/sendmail.xml;echo ' <SourcedFixletAction>' >>/tmp/sendmail.xml;echo '   <SourceFixlet>' >>/tmp/sendmail.xml;echo '     <Sitename>BCP Innovation Automata</Sitename>' >>/tmp/sendmail.xml;echo '     <FixletID>52525</FixletID>' >>/tmp/sendmail.xml;echo '     <Action>Action1</Action>' >>/tmp/sendmail.xml; echo '   </SourceFixlet>' >>/tmp/sendmail.xml;echo '   <Target>' >>/tmp/sendmail.xml;echo '     <ComputerName>PADMINICOP01</ComputerName>' >>/tmp/sendmail.xml;echo '   </Target>' >>/tmp/sendmail.xml;echo '   <Parameter Name="PARA">'$1'</Parameter>' >>/tmp/sendmail.xml;echo '   <Parameter Name="ASUNTO">'$2'</Parameter>' >>/tmp/sendmail.xml;echo '   <Parameter Name="SERVIDOR">'$3'</Parameter>' >>/tmp/sendmail.xml;echo '   <Parameter Name="IP">'$4'</Parameter>' >>/tmp/sendmail.xml;echo '   <Parameter Name="PASSWORD">'$5'</Parameter>' >>/tmp/sendmail.xml;echo '   <Parameter Name="MATRICULA">'$6'</Parameter>' >>/tmp/sendmail.xml;echo ' </SourcedFixletAction>' >>/tmp/sendmail.xml;echo '</BES>' >>/tmp/sendmail.xml;}
   host=$(hostname | cut -d '.' -f1)
   #rootpass=$(openssl rand -hex 4)
   #
   #Cambio pwd a root
   #echo "root:$rootpass" | chpasswd
   IP=$(ifconfig | grep -A 1 'ens192' | tail -1 | cut -d 't' -f2 | cut -d 'n' -f1 | cut -d ' ' -f2 | cut -d ' ' -f1)
   #sendmail $netsec_mail "Notificacion de cambio de password" $host $IP $rootpass root
   #curl -X POST --data-binary @/tmp/sendmail.xml --user iemapp:h4bahihx -k https://10.80.129.152:52311/api/actions
   #
   #Envio de pwd a usuarios de LBS
   #i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1) correo=$(echo $line | cut -d ',' -f2) password=$(echo $line | cut -d ',' -f3);if [ -n "$correo" ];then sendmail $correo "Envio de credenciales" $host $IP $password $matricula;curl -X POST --data-binary @/tmp/sendmail.xml --user iemapp:h4bahihx -k https://10.80.129.152:52311/api/actions;fi;done < "/tmp/passwords.txt"
   i=0; while read line; do matricula=$(echo $line | cut -d ',' -f1) correo=$(echo $line | cut -d ',' -f2) password=$(echo $line | cut -d ',' -f3);if [ -n "$correo" ];then curl http://10.80.200.198:20000/sendemail?to=$correo\\&subject=ICO%20PRODUCCION:%20Envio%20de%20credenciales\\&message=Se%20le%20informa%20que%20sus%20credenciales%20en%20el%20servidor%20$host%20con%20IP%20$IP%20es%20la%20siguiente%3A%20%3Cbr%3E%3Cbr%3E%20usuario%3A$matricula%20%3Cbr%3E%20password%3A$password\\&apikey=0xfwb9030w=m08;sleep 3;fi;done < "/tmp/passwords.txt" &
   #
   #Guardar Logs
   ls -tr /tmp/rhel* | while read fn ; do cat "$fn" >> /var/log/rhel7_recipe_steps.log; done
   rm -f /tmp/*.txt
  EOH
  not_if { ::File.exist?('/tmp/rhel7_007_base_setCredentials_01.log') }
end
