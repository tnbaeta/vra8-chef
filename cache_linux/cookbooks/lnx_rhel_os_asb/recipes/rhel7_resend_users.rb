#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_005_base_setUsers
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_005_base_setUsers" do
  level :info
end

bash "reseteo_masivo" do
  cwd "/tmp"
  code <<-EOH
   #Cambio de passwords
   i=0; while read line; do matricula=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f4) password=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f3);echo "$matricula:$password" | chpasswd;done < "/fixes/lbs/usuarios-bcp.sh"
   i=0; while read line; do matricula=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f4);chage -d 0 $matricula;done < "/fixes/lbs/usuarios-bcp.sh"
   i=0; while read line; do matricula=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f4) password=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f3);echo "$matricula:$password" | chpasswd;done < "/fixes/lbs/usuarios-ibm.sh"
   i=0; while read line; do matricula=$(echo $line | cut -d '"' -f3 | cut -d "/" -f5 | cut -d ' ' -f4);chage -d 0 $matricula;done < "/fixes/lbs/usuarios-ibm.sh"
  EOH
  only_if { ::File.exist?('/fixes/lbs/usuarios-ibm.sh') }
end


