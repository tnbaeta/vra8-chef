#
# Cookbook Name:: lnx_rhel_os
# Recipe:: rhel7_008_base_setVMwareTools
#
# Recipe Format Platform_Order_Enviroment_Task
#
# Copyright 2017, IBM
#
# All rights reserved - Do Not Redistribute
#
log "rhel7_008_base_setVMwareTools" do
  level :info
end

bash "rhel7_008_base_setVMwareTools_01" do
  cwd "/tmp"
  code <<-EOH
   #Descomprimir VMware Tools
   mkdir -p /fixes/vmware
   cd /fixes/vmware
   rm -fr /tmp/VMwareTools-8.6.15-2495133.tar.gz
   vmtoolspath=`ls /tmp/VMwareTools*`
   #vmtoolspath=$vmtoolspath  | cut -d ' ' -f 1
   tar zxpf $vmtoolspath
  EOH
  not_if { ::File.exist?('/tmp/rhel7_008_base_setVMwareTools_01.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se extrae VMtools\" >> /tmp/rhel7_008_base_setVMwareTools_01.log"
end

bash "rhel7_008_base_setVMwareTools_02" do
  cwd "/tmp"
  code <<-EOH
   #Instalar VMware Tools
   /fixes/vmware/vmware-tools-distrib/vmware-install.pl --default
  EOH
  not_if { ::File.exist?('/tmp/rhel7_008_base_setVMwareTools_02.log') }
end

execute 'Local Log01' do
  command "echo \" `date` : Se instala VMtools\" >> /tmp/rhel7_008_base_setVMwareTools_02.log"
end