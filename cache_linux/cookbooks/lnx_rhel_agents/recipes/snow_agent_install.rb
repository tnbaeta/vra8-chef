#
# Cookbook Name:: lnx_rhel_agents
# Recipe:: snow_agent_install
# Author:: Joel Rodriguez Cerin
# Copyright 2018, IBM
#
# Fecha Creacion:: 07.12.2018
# All rights reserved - Do Not Redistribute
# Environment Production

#Elimina archivos temporales
file '/opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm' do
  action :delete
end

directory '/tmp/nimldr_linux/' do
  recursive true
  action :delete
end

#Descarga RPM de Agente
cookbook_file '/opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm' do
 source 'bancodecreditodelperu_snowagent_520_1_x86_64.rpm'
 action :create
 mode '0755'
 not_if { ::File.exist?('/opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm') }
end

bash "instalacion de agente snow" do
  cwd "/tmp"
  code <<-EOH
	echo "Instaladores copiados" >> "/tmp/trace-Snow.log"
	rpm -ivh /opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm >> "/tmp/instala_robotSnow.log"
	echo "Se ejecuta instalador" >> "/tmp/trace-Snow.log"
	rm -f /opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm
	echo "Se eliminan instaladores" >> "/tmp/trace-Snow.log"
  EOH
  only_if { ::File.exist?('/opt/bancodecreditodelperu_snowagent_520_1_x86_64.rpm') }
end

