#
# Cookbook Name:: lnx_rhel_os_asb
# Recipe:: uim_agent_install
# Author:: Jorge Neira Chang
# Copyright 2018, IBM
#
# All rights reserved - Do Not Redistribute
# Environment Production

#Elimina archivos temporales
file '/opt/nms-robot-vars.cfg' do
  action :delete
end

file '/opt/nimsoft-robot.x86_64.rpm' do
  action :delete
end

file '/tmp/trace-UIM.log' do
  action :delete
end

file '/tmp/instala_robot.log' do
  action :delete
end

directory '/tmp/nimldr_linux/' do
  recursive true
  action :delete
end

#Descarga CFG de Agente
cookbook_file '/opt/nms-robot-vars.cfg' do
 source 'nms-robot-vars.cfg'
 action :create
 mode '0755'
 not_if { ::File.exist?('/opt/nms-robot-vars.cfg') }
end

#Descarga RPM de Agente
cookbook_file '/opt/nimsoft-robot.x86_64.rpm' do
 source 'nimsoft-robot.x86_64.rpm'
 action :create
 mode '0755'
 not_if { ::File.exist?('/opt/nimsoft-robot.x86_64.rpm') }
end

bash "instalacion de agente" do
  cwd "/tmp"
  code <<-EOH
	echo "Instaladores copiados" >> "/tmp/trace-UIM.log"
	host=
	ip=
	echo -e "\nROBOTIP=#{node["ipaddress"]}" >> "/opt/nms-robot-vars.cfg"
	echo ROBOTNAME="#{node["hostname"]}" >> "/opt/nms-robot-vars.cfg"
	echo "Archivo de configuracion editado" >> "/tmp/trace-UIM.log"
	rpm -ivh /opt/nimsoft-robot.x86_64.rpm >> "/tmp/instala_robot.log"
	echo "Se ejecuta instalador" >> "/tmp/trace-UIM.log"
	bash /opt/nimsoft/install/RobotConfigurer.sh
	echo "Se configura el robot" >> "/tmp/trace-UIM.log"
	/etc/init.d/nimbus start
	echo "Se inicia el robot" >> "/tmp/trace-UIM.log"
	rm -f /opt/rm nimsoft-robot.x86_64.rpm
	echo "Se eliminan instaladores" >> "/tmp/trace-UIM.log"
  EOH
  only_if { ::File.exist?('/opt/nms-robot-vars.cfg') }
end

